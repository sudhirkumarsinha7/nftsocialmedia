/**
 * @type import('hardhat/config').HardhatUserConfig
 */
require("@nomiclabs/hardhat-waffle");
require("dotenv/config");

const { HARDHAT_PORT } = process.env;

module.exports = {
  solidity: "0.7.3",
  networks: {
    localhost: { url: `http://127.0.0.1:${HARDHAT_PORT}` },
    hardhat: {
      accounts: [{"privateKey":"0x19b00eb88a83f8477acbcbf34b297dd03da62a98cb5aec616bc678d42615b740","balance":"1000000000000000000000"},{"privateKey":"0x2103606d9d8fc4f85fb515982a2dedc13e8e4cdf2e88175211685cb058a9e4f1","balance":"1000000000000000000000"},{"privateKey":"0x260e9fd5070fb16b30768015303999a599653973e97677a14fe61723364b6de2","balance":"1000000000000000000000"},{"privateKey":"0x4d25c571e436a42c88ca44d119338b2015fd9f6aa1d917ee49e4205fe8f62b16","balance":"1000000000000000000000"},{"privateKey":"0xc0021f3da7e53000612014ce1cc140b60afc552b827a63716e8a97299727148b","balance":"1000000000000000000000"},{"privateKey":"0x594a304f9c11216525b3fdab4846d91b31fc55751d83e32c17ee3f4ff39e4729","balance":"1000000000000000000000"},{"privateKey":"0xafacf7f0806cf1f3486e7d45d8ebebe2654d7f03e5dbba29d493c57cff64f31f","balance":"1000000000000000000000"},{"privateKey":"0x772116922b1f58e9a5a688dcd09db0d1d1ab3bab22eea064151495c1233e6c20","balance":"1000000000000000000000"},{"privateKey":"0x000863bd0a71ed70dd43efa4d73adfc87134856ad32c60c683eb25a22ca3e858","balance":"1000000000000000000000"},{"privateKey":"0x1fd8cc9135ba71caf481d37bc25d9826dc040862ff8f40ea8338b40ae46c2eaf","balance":"1000000000000000000000"}]
    },
  },
  paths: {
    sources: './contracts',
    tests: './__tests__/contracts',
    cache: './cache',
    artifacts: './artifacts',
  },
};