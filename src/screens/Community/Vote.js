/* eslint-disable react-native/no-color-literals */
/* eslint-disable sort-keys */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react';
import { useColorScheme, StyleSheet, Platform, Text, View, Image, TextInput, FlatList, ScrollView, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { scale,verticalScale } from '../../components/Scale';
import { localImage, imageslist, DeviceWidth } from '../../config/global';
import Feather from 'react-native-vector-icons/Feather';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import AntDesign from 'react-native-vector-icons/AntDesign'
import { useIsFocused } from '@react-navigation/native';
import { Colors } from 'react-native-paper';
import { CustomLinearBotton, CustomLinearBotton1 } from '../../components/Common/Button';
import { Header } from '../../components/Header'
import { useTheme } from '../../theme/Hooks'
import {SelectionSelectOption} from '../../components/SelectionSelect'

let radioItems = [
  {
    label: 'Sure',
    selected: false,
  },
  {
    label: "Not Sure",
    selected: false,
  },
  {
    label:  "Don't want to say",
    selected: false,
  },
]
const Vote = props => {
  const isFocused = useIsFocused();
  const {  NavigationTheme } = useTheme();
  const {colors}=NavigationTheme;
  const [selectedTab, setTab] = useState('');

  const changeActiveRadioButton = (index) => {
    radioItems.map(item => {
      item.selected = false;
    });

    radioItems[index].selected = true;
    setTab(radioItems[index].label)
  }
  useEffect(() => {

  }, []);
const imageDetails =()=>{
  return   <View style={{}}>
     <View style={{ flexDirection: 'row',backgroundColor:'#84FFE9',padding:10,borderRadius:10,alignItems:'center' }}>
        <View style={{  }}>
          <Image source={localImage.Frame}
            resizeMode="stretch"
            style={{ width: scale(20), height: scale(20), borderRadius: 15 }}
          />
 
        </View>
        <View style={{ flex: 8,marginLeft:scale(10) }}>

          <Text style={{ fontSize: scale(16), fontWeight: '400', }}>{'Aditi Musunur'}</Text>
        </View>
        <TouchableOpacity style={{backgroundColor:'#9225FF',borderRadius:10,padding:7}} onPress={() => alert('not implemented yet')}>

        <Text style={{ fontSize: scale(16), fontWeight: '400', }}>{'Join'}</Text>

        </TouchableOpacity>
      </View>
  <View style={{ flexDirection: 'row', marginTop: 10, alignItems: 'center' }}>
      <View style={{ flexDirection: 'row', flex: 0.7, alignItems: 'center' }}>
      <Text style={{ color: colors.text, fontSize: scale(12), fontWeight: '400',}}>{'21 hours Ago'}</Text>

      </View>
    </View>
    <View style={{ alignItems: 'center',marginTop:scale(10),marginBottom:scale(10) }}>
      <Text style={{ color: colors.text, fontSize: scale(14), fontWeight: '400', }}>{'Have a great day with my amazing client allthe way from NewYork'}</Text>
    </View>
    <Image source={localImage.Frame}
      resizeMode={'stretch'}
      style={{ width: DeviceWidth / 1.1, height: scale(200), }}
    />
    <View style={{ flexDirection:'row',justifyContent:'space-between',marginTop:scale(10),borderBottomWidth:0.5,borderColor:colors.text,paddingBottom:scale(10)}}>
     
    <View style={{ flexDirection: 'row',alignItems: 'center' }}>
        <AntDesign name='arrowup' color={'red'} size={25} />
        <Text style={{ color: colors.text, fontSize: scale(12), fontWeight: '400', padding: 10, }}>{'56.9K'}</Text>
      </View>
      <View style={{ flexDirection: 'row',alignItems: 'center' }}>
        <AntDesign name='message1' color={colors.text} size={25} />
        <Text style={{ color: colors.text, fontSize: scale(12), fontWeight: '400', padding: 10, }}>{'3338'}</Text>
      </View>
      <View style={{ flexDirection: 'row',alignItems: 'center' }}>
        <AntDesign name='save' color={'red'} size={25} />
      </View>
      <View style={{ flexDirection: 'row',alignItems: 'center' }}>
        <Feather name='share' color={colors.text} size={25} />
      </View>
    </View>
    <View style={{ flexDirection: 'row', marginTop: 10, alignItems: 'center' }}>
      <View style={{ flexDirection: 'row', flex: 0.7, alignItems: 'center' }}>
      <Text style={{ color: colors.text, fontSize: scale(12), fontWeight: '400',}}>{'21 hours Ago'}</Text>

      </View>
    </View>
    <View style={{ alignItems: 'center',marginTop:scale(10) }}>
      <Text style={{ color: colors.text, fontSize: scale(14), fontWeight: '400', }}>{'Have a great day with my amazing client allthe way from NewYork'}</Text>
    </View>
  </View>
}

  return (
    <View >
      <Header
        navigation={props.navigation}
        name={'Discover'}
      />
      <ScrollView style={{ margin: scale(15) }}>

        
        
        <View style={{ alignItems: 'center' }}>
          
          {imageDetails()}

        
    

        </View>
        <View style={{ marginTop:20}}>

        {radioItems.map((item, key) => (
          <SelectionSelectOption
            key={key}
            button={item}
            onClick={() => changeActiveRadioButton(key)}
            selectedTextColor={'#84FFE9'}
            unSelectedTextColor={'green'}
          />
        ))}
        </View>
        <CustomLinearBotton
        title={'Vote'}
        onNavigate={() => alert('Not Implemented')}
      />
       <CustomLinearBotton
        title={'Skip Vote'}
        onNavigate={() =>alert('Not Implemented')}
      />
        <View style={{ height: DeviceWidth }} />
      </ScrollView>


    </View>
  );
};

const styles = StyleSheet.create({
  pageContainer: {
    marginTop: Platform.OS === 'ios' ? scale(40) : 0
  },
});

function mapStateToProps(state) {
  return {


  };
}

export default connect(mapStateToProps, {

})(Vote);
