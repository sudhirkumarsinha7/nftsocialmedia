import * as React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Community from './index';
import Screen1 from './screen1';
import ExploreCommunity from './ExploreCommunity';
import JoinCommunity from './JoinCommunity';
import FaqSupport from './FaqSupport';
import Discover from './Discover';
import Vote from './Vote';

const Stack = createNativeStackNavigator();
const Communitytack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Community"
        component={Community}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="ExploreCommunity"
        component={ExploreCommunity}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="JoinCommunity"
        component={JoinCommunity}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="FaqSupport"
        component={FaqSupport}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="Discover"
        component={Discover}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="Vote"
        component={Vote}
        options={{
          headerShown: false,
        }}
      />
    </Stack.Navigator>
  );
};
export default Communitytack;
