/* eslint-disable react-native/no-color-literals */
/* eslint-disable sort-keys */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react';
import { useColorScheme, StyleSheet, Platform, Text, View, Image, TextInput, FlatList, ScrollView, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { scale } from '../../components/Scale';
import { localImage, imageslist, DeviceWidth } from '../../config/global';
import Feather from 'react-native-vector-icons/Feather';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import AntDesign from 'react-native-vector-icons/AntDesign'
import { useIsFocused } from '@react-navigation/native';
import { Colors } from 'react-native-paper';
import { CustomLinearBotton, CustomLinearBotton1 } from '../../components/Common/Button';
import { Header } from '../../components/Header'
import { textColor } from '../../components/Common/theme'
import { useSafeAreaFrame } from 'react-native-safe-area-context';
import { useTheme } from '../../theme/Hooks'

const Discover = props => {
  const isFocused = useIsFocused();
  const {  NavigationTheme } = useTheme();
  const {colors}=NavigationTheme;
  useEffect(() => {

  }, []);


  return (
    <View >
      <Header
        navigation={props.navigation}
        name={'Discover'}
      />
      <ScrollView style={{ margin: scale(15) }}>

        <View style={{ marginTop: scale(15), }}>

          <Text style={{ color: colors.text, fontSize: scale(20), fontWeight: '700', textAlign: 'center' }}>{'About Communiti Platform'}</Text>
          <View style={{ padding: 30 }}>
            <Image
              source={require('../../assets/Img/processor.png')}
              style={{ height: 200, width: 250, }}
            />
          </View>
          <Text style={{ color: colors.text, fontSize: scale(14), fontWeight: '400', marginTop: scale(10), textAlign: 'center' }}>{'Communiti help anyone create a beautiful website, landing page, app to collect NFTs digital art'}</Text>
          <Text style={{ color: colors.text, fontSize: scale(20), fontWeight: '700', marginTop: scale(20), }}>{'Crypto for Creative Communities'}</Text>

          <Text style={{ color: colors.text, fontSize: scale(14), fontWeight: '400', marginTop: scale(10), marginLeft: scale(10) }}>{'NFTs—non-fungible tokens—are empowering artists, musicians, and all kinds of genre-defying digital creators to invent a new cultural paradigm. How this emerging culture of digital art ownership looks is up to all of us.'}</Text>

          <Text style={{ color: colors.text, fontSize: scale(20), fontWeight: '700', marginTop: scale(20), }}>{'How it work'}</Text>

          <View style={{ padding: 30, alignSelf: 'center', flexDirection: 'row' }}>
            <View style={{ backgroundColor: colors.card, borderRadius: 10, margin: 5, padding: 10 }}>
              <Image
                source={require('../../assets/Img/globe.png')}
                style={{ height: 150, width: 150, }}
              />
              <Text style={{ color: colors.text, fontSize: scale(16), fontWeight: '700', textAlign: 'center', margin: 7 }}>{'Build together'}</Text>

            </View>
            <View style={{ backgroundColor: colors.card, borderRadius: 10, margin: 5, padding: 10 }}>
              <Image
                source={require('../../assets/Img/heart.png')}
                style={{ height: 150, width: 150, }}
              />
              <Text style={{ color: colors.text, fontSize: scale(16), fontWeight: '700', textAlign: 'center', margin: 7 }}>{'Trust'}</Text>

            </View>
          </View>
          <Text style={{ color: colors.text, fontSize: scale(16), fontWeight: '700', marginTop: scale(20), }}>{'For Creators'}</Text>
          <Text style={{ color: colors.text, fontSize: scale(14), fontWeight: '400', }}>{'Creators are invited to join Foundation by members of the community. Once you’ve received an invite, you’ll need to set up a MetaMask wallet with ETH before you can create an artist profile and mint an NFT—which means uploading your JPG, PNG, or video file to IPFS, a decentralized peer-to-peer storage network. It will then be an NFT you can price in ETH and put up for auction on Foundation. '}</Text>

          <Text style={{ color: colors.text, fontSize: scale(16), fontWeight: '700', marginTop: scale(20), }}>{'For Collectors '}</Text>
          <Text style={{ color: colors.text, fontSize: scale(14), fontWeight: '400', }}>{'On Foundation, anyone can create a profile to start collecting NFTs. All you’ll need is a MetaMask wallet and ETH, the cryptocurrency used to pay for all transactions on Ethereum. Artists list NFTs for auction at a reserve price, and once the first bid is placed, a 24-hour auction countdown begins. If a bid is placed within the last 15 minutes, the auction extends for another 15 minutes. '}</Text>


        </View>
        <View style={{ alignItems: 'center' }}>

       
        <Image
          style={{ height: 60, width: DeviceWidth / 2 }}
          resizeMode="stretch"
          source={require('../../assets/Img/logo.png')}
        />
        <Text style={{ color: colors.text, fontSize: scale(22), fontWeight: '700', textAlign: 'center' }}>{'The New NFT MARKETPLACE'}</Text>

      </View>

      <CustomLinearBotton
        title={'Discover more'}
        onNavigate={() => props.navigation.navigate('Discover')}
      />
      <CustomLinearBotton1
        title={'FAQ’s & Support'}
        onNavigate={() => props.navigation.navigate('ExploreCommunity')}
      />
        <View style={{ height: DeviceWidth }} />
      </ScrollView>


    </View>
  );
};

const styles = StyleSheet.create({
  pageContainer: {
    marginTop: Platform.OS === 'ios' ? scale(40) : 0
  },
});

function mapStateToProps(state) {
  return {


  };
}

export default connect(mapStateToProps, {

})(Discover);
