/* eslint-disable react-native/no-color-literals */
/* eslint-disable sort-keys */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react';
import { useColorScheme, StyleSheet, Platform, Text, View, Image, TextInput,FlatList,ScrollView } from 'react-native';
import { connect } from 'react-redux';
import { scale } from '../../components/Scale';
import { localImage, imageslist, DeviceWidth } from '../../config/global';
import Feather from 'react-native-vector-icons/Feather';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import AntDesign  from 'react-native-vector-icons/AntDesign'
import { useIsFocused } from '@react-navigation/native';
import { Colors } from 'react-native-paper';
import { CustomLinearBotton,CustomLinearBotton1 } from '../../components/Common/Button';
import { Header } from '../../components/Header'
import { textColor } from '../../components/Common/theme'
import { useSafeAreaFrame } from 'react-native-safe-area-context';
import { useTheme } from '../../theme/Hooks'

const HomeScreen = props => {
  const isFocused = useIsFocused();
  const {  NavigationTheme } = useTheme();
  const {colors}=NavigationTheme;
  useEffect(() => {

  }, []);
 

  return (
    <ScrollView >
      <Header
        navigation={props.navigation}
        name={'Home'}
      />
      <View style={{ marginTop: scale(20) }}>

        <Text style={{ color: colors.text, fontSize: scale(18), fontWeight: '700', textAlign: 'center' }}>{'Join the Community to access your favouriate NFT’s.'}</Text>
        <Text style={{ color: colors.text, fontSize: scale(16), fontWeight: '400', textAlign: 'center', marginTop: scale(15) }}>{'With the Community, Users are encouraged to support other artists and to set the stage for a model of community-led curation that puts power in the hands of creators.'}</Text>
        

      </View>

     <CustomLinearBotton 
     title={'Join Community'}
     />
           <View style={{alignItems:'center' }}>

      <Image
                    source={require('../../assets/Img/globe.png')}
                    style={{height:200,width:150}}
                />
                           <Text style={{ color: colors.text, fontSize: scale(18), fontWeight: '400', textAlign: 'center' }}>{'Current number of profiles on the Community'}</Text>

                <Image
            style={{ height: 60,width:DeviceWidth/1.4}}
            resizeMode="stretch"
            source={require('../../assets/Img/logo.png')}
          />
                  <Text style={{ color: colors.text, fontSize: scale(22), fontWeight: '700', textAlign: 'center' }}>{'The New NFT MARKETPLACE'}</Text>

                </View>

<CustomLinearBotton 
     title={'Discover more'}
     />
     <CustomLinearBotton1 
     title={'FAQ’s & Support'}
     />
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  pageContainer: {
    marginTop: Platform.OS === 'ios' ? scale(40) : 0
  },
});

// export default HomeScreen;
function mapStateToProps(state) {
  return {


  };
}

export default connect(mapStateToProps, {

})(HomeScreen);
