/* eslint-disable react-native/no-color-literals */
/* eslint-disable sort-keys */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react';
import { useColorScheme, StyleSheet, Platform, Text, View, Image, TextInput, FlatList, ScrollView, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { scale } from '../../components/Scale';
import { localImage, imageslist, DeviceWidth } from '../../config/global';
import Feather from 'react-native-vector-icons/Feather';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import AntDesign from 'react-native-vector-icons/AntDesign'
import { useIsFocused } from '@react-navigation/native';
import { Colors } from 'react-native-paper';
import { CustomLinearBotton, CustomLinearBotton1 } from '../../components/Common/Button';
import { Header } from '../../components/Header'
import { useTheme } from '../../theme/Hooks'

const CustomView = (props) => {
  return <View style={{ backgroundColor: props.backgroundColor, padding: scale(10), borderRadius: scale(8), flexDirection: 'row', marginTop: 15 }}>
    <Text style={{ color: props.textColor, fontSize: scale(18), fontWeight: '400', flex: 0.98, marginLeft: scale(10) }}>{props.name}</Text>
    <AntDesign name="caretdown" size={18} color={'#DADADA'} />
  </View>
}
const FaqSupport = props => {
  const isFocused = useIsFocused();
  const {  NavigationTheme } = useTheme();
  const {colors}=NavigationTheme;
  useEffect(() => {

  }, []);


  return (
    <View >
      <Header
        navigation={props.navigation}
        name={'Discover'}
      />
      <ScrollView style={{ margin: scale(15) }}>

        <View style={{ marginTop: scale(15), }}>

          <Text style={{ color: colors.text, fontSize: scale(20), fontWeight: '700', textAlign: 'center' }}>{'Frequently asked questions'}</Text>

          <Text style={{ color: colors.text, fontSize: scale(14), fontWeight: '400', marginTop: scale(10), textAlign: 'center' }}>{'Join our community now to get free updates and also alot of freebies are waiting for you or Contact Support'}</Text>

        </View>
        <CustomView
          textColor={colors.text} backgroundColor={colors.card}
          name={'General'} />
        <CustomView name={'How does it work'}  textColor={colors.text} backgroundColor={colors.card}/>
        <CustomView name={'How to start'}  textColor={colors.text} backgroundColor={colors.card}/>
        <CustomView name={'How to payment'} textColor={colors.text} backgroundColor={colors.card}/>
        <CustomView name={'How to bid'}  textColor={colors.text} backgroundColor={colors.card}/>

        <View style={{ alignItems: 'center' }}>
          <Image
            style={{ height: 60, width: DeviceWidth / 2 }}
            resizeMode="stretch"
            source={require('../../assets/Img/logo.png')}
          />
          <Text style={{ color: colors.text, fontSize: scale(22), fontWeight: '700', textAlign: 'center' }}>{'The New NFT MARKETPLACE'}</Text>

        </View>

        <CustomLinearBotton
          title={'Discover more'}
          onNavigate={() => props.navigation.navigate('Discover')}
        />
        <CustomLinearBotton1
          title={'FAQ’s & Support'}
          onNavigate={() => props.navigation.navigate('ExploreCommunity')}
        />
        <View style={{ height: DeviceWidth }} />
      </ScrollView>


    </View>
  );
};

const styles = StyleSheet.create({
  pageContainer: {
    marginTop: Platform.OS === 'ios' ? scale(40) : 0
  },
});

function mapStateToProps(state) {
  return {


  };
}

export default connect(mapStateToProps, {

})(FaqSupport);
