/* eslint-disable react-native/no-color-literals */
/* eslint-disable sort-keys */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react';
import { useColorScheme, StyleSheet, Platform, Text, View, Image, TextInput, FlatList, ScrollView, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { scale } from '../../components/Scale';
import { localImage, ModalList, DeviceWidth } from '../../config/global';
import Feather from 'react-native-vector-icons/Feather';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import AntDesign from 'react-native-vector-icons/AntDesign'
import { useIsFocused } from '@react-navigation/native';
import { Colors } from 'react-native-paper';
import { CustomLinearBotton, CustomLinearBotton1 } from '../../components/Common/Button';
import { Header } from '../../components/Header'
import { textColor } from '../../components/Common/theme'
import { useSafeAreaFrame } from 'react-native-safe-area-context';
import { useTheme } from '../../theme/Hooks'

const ExploreCommunityScreen = props => {
  const isFocused = useIsFocused();
  const {  NavigationTheme } = useTheme();
  const {colors}=NavigationTheme;
  useEffect(() => {

  }, []);
  const eachArtist = (item) => {
    return <View style={{ padding: 15, borderRadius: 20, margin: scale(10),backgroundColor:colors.text }}>
      <TouchableOpacity onPress={()=>props.navigation.navigate('Vote')}>
        <Image source={item.image}
                resizeMode={'stretch'}

          style={{ width: scale(70), height: scale(70), borderRadius: 40,}}
        />

      </TouchableOpacity>
      
    </View>
  }

  return (
    <View >
      <Header
        navigation={props.navigation}
        name={'ExploreCommunityScreen'}
      />
      <View style={{ marginTop: scale(15), flexDirection: 'row', alignItems: 'center' }}>

        <Text style={{ color: '#32BEA6', fontSize: scale(24), fontWeight: '700', flex: 1 }}>{'COMMUNITY'}</Text>
        <TouchableOpacity
          onPress={() => alert('Not Implemented yet')}
        >
          <AntDesign name="arrowright" size={40} color={colors.text} />
        </TouchableOpacity>
      </View>
      <View style={{ marginTop: scale(5), marginLeft: scale(20) }}>

        <Text style={{ color: colors.text, fontSize: scale(16), fontWeight: '700', }}>{'Let’s Get Started'}</Text>
        <Text style={{ color: '#D9D9D9', fontSize: scale(14), fontWeight: '400', marginTop: scale(10) }}>{'PICK YOUR FAVOURITE ARTIST AND JOIN MILLIONS OF OTHER USERS.'}</Text>

      </View>
      <FlatList
        // onEndReachedThreshold={0.7}
        horizontal={false}
        numColumns={3}
        keyExtractor={(item, index) => index.toString()}
        data={ModalList}
        renderItem={({ item }) => eachArtist(item)}
        ListFooterComponent={<View style={{ height: DeviceWidth }} />}

      />
    </View>
  );
};

const styles = StyleSheet.create({
  pageContainer: {
    marginTop: Platform.OS === 'ios' ? scale(40) : 0
  },
});

function mapStateToProps(state) {
  return {


  };
}

export default connect(mapStateToProps, {

})(ExploreCommunityScreen);
