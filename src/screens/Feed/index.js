/* eslint-disable react-native/no-color-literals */
/* eslint-disable sort-keys */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react';
import { useColorScheme, StyleSheet, Platform, Text, View, Image, TouchableOpacity, FlatList, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import { scale } from '../../components/Scale';
import { ModalList1, GallaryList, DeviceWidth } from '../../config/global';
import Feather from 'react-native-vector-icons/Feather';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import AntDesign from 'react-native-vector-icons/AntDesign'
import { useIsFocused } from '@react-navigation/native';
import { Colors } from 'react-native-paper';
import { CustomLinearBotton, CustomLinearBotton1 } from '../../components/Common/Button';
import { Header } from '../../components/Header'
import { useTheme } from '../../theme/Hooks'
import InstaStory from 'react-native-insta-story';
const data = [
  {
    user_id: 1,
    user_image:
      'https://pbs.twimg.com/profile_images/1222140802475773952/61OmyINj.jpg',
    user_name: 'Sudhir Kumar',
    stories: [
      {
        story_id: 1,
        story_image:
          'https://image.freepik.com/free-vector/universe-mobile-wallpaper-with-planets_79603-600.jpg',
        swipeText: 'Custom swipe text for this story',
        onPress: () => console.log('story 1 swiped'),
      },
      {
        story_id: 2,
        story_image:
          'https://image.freepik.com/free-vector/mobile-wallpaper-with-fluid-shapes_79603-601.jpg',
      },
    ],
  },
  {
    user_id: 2,
    user_image:
      'https://images.unsplash.com/photo-1511367461989-f85a21fda167?ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8cHJvZmlsZXxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&w=1000&q=80',
    user_name: 'Rajeev Ranjan',
    stories: [
      {
        story_id: 1,
        story_image:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTjORKvjcbMRGYPR3QIs3MofoWkD4wHzRd_eg&usqp=CAU',
        swipeText: 'Custom swipe text for this story',
        onPress: () => console.log('story 1 swiped'),
      },
      {
        story_id: 2,
        story_image:
          'https://files.oyebesmartest.com/uploads/preview/vivo-u20-mobile-wallpaper-full-hd-(1)qm6qyz9v60.jpg',
        swipeText: 'Custom swipe text for this story',
        onPress: () => console.log('story 2 swiped'),
      },
    ],
  },
];
const FeedScreen = props => {
  const isFocused = useIsFocused();
  const { NavigationTheme } = useTheme();
  const { colors } = NavigationTheme;
  useEffect(() => {

  }, []);

  const eachSaleList = (item) => {
    return <View style={{ borderRadius: 10, padding: 10, borderBottomWidth: 0.5, borderBottomColor: '#363636' }}>

      <View style={{ flex: 1, flexDirection: 'row' }}>
        <View style={{ flex: 1 }}>
          <Image source={item.image}
            resizeMode="stretch"
            style={{ width: scale(20), height: scale(20), borderRadius: 15 }}
          />
 
        </View>
        <View style={{ flex: 9 }}>

          <Text style={{ color: colors.text, fontSize: scale(16), fontWeight: '400', }}>{'Aditi Musunur'}</Text>
        </View>
        <TouchableOpacity style={{}} onPress={() => alert('not implemented yet')}>

          <MaterialCommunityIcons name='dots-vertical' color={colors.text} size={25} />
        </TouchableOpacity>
      </View>
      <View style={{ flex: 1, marginTop: 15 }}>
        <Image source={item.image}
          resizeMode={'stretch'}
          style={{ width: DeviceWidth / 1.1, height: scale(200), }}
        />
        <View style={{ backgroundColor: '#84FFE9', width: DeviceWidth / 1.1, }}>
          <Text style={{ fontSize: scale(12), fontWeight: '400', padding: 10, }}>{'Buy the Nft'}</Text>

        </View>
        <View style={{ flexDirection: 'row', marginTop: 10, alignItems: 'center' }}>
          <View style={{ flexDirection: 'row', flex: 0.7, alignItems: 'center' }}>
            <AntDesign name='hearto' color={'red'} size={25} />
            <Text style={{ color: colors.text, fontSize: scale(12), fontWeight: '400', padding: 10, }}>{'28,78,878'}</Text>
          </View>
          <View style={{ flexDirection: 'row', flex: 0.3 }}>
            <Text style={{ color: colors.text, fontSize: scale(12), fontWeight: '400', padding: 10, }}>{'21 hours Ago'}</Text>

          </View>


        </View>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <Text style={{ color: colors.text, fontSize: scale(12), fontWeight: '900', }}>{'Barasati Sandipa'}</Text>
          <Text style={{ color: colors.text, fontSize: scale(12), fontWeight: '400', }}>{' Tum Hi Ho Song by Arijit Singh...'}</Text>

        </View>
      </View>
    </View>
  }

  const HomeScreen=(item)=>{
    return <View>
     <FlatList
            // onEndReachedThreshold={0.7}
            horizontal={false}
            keyExtractor={(item, index) => index.toString()}
            data={GallaryList}
            renderItem={({ item }) => eachSaleList(item)}

          />
    </View>
  }

  return (
    <View >
      <Header
        navigation={props.navigation}
        name={'Home'}
      />
      <View style={{ margin: 10 }}>

        <View style={{marginTop:-30}}>
          <InstaStory
            data={data}
            duration={10}
            onStart={(item) => console.log(item)}
            onClose={(item) => console.log('close: ', item)}
            textStyle={{ color: colors.text }}
            customSwipeUpComponent={
              <View>
               <Text style={{ color: colors.text, fontSize: scale(16), fontWeight: '400', }}>swipe</Text>
              </View>
            }
            style={{ marginTop: 30 }}
          />
        </View>

        <FlatList
            onEndReachedThreshold={0.7}
            style={{ width: '100%' }}
            keyExtractor={(item, index) => index.toString()}
            data={['test']}
            renderItem={({ item, index }) => HomeScreen(item, index)}
            ListFooterComponent={<View style={{ height: DeviceWidth }} />}

        />
        
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  pageContainer: {
    marginTop: Platform.OS === 'ios' ? scale(40) : 0
  },
});

// export default FeedScreen;
function mapStateToProps(state) {
  return {


  };
}

export default connect(mapStateToProps, {

})(FeedScreen);
