import {createNativeStackNavigator} from '@react-navigation/native-stack';
import * as React from 'react';


import Feed from './index';
const Stack = createNativeStackNavigator();
const FeedScreen = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Feed"
        component={Feed}
        options={{
          headerShown: false,
        }}
      />
    </Stack.Navigator>
  );
};
export default FeedScreen;
