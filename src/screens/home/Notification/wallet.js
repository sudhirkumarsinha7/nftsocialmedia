/* eslint-disable react-native/no-color-literals */
/* eslint-disable sort-keys */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react';
import { useColorScheme, StyleSheet, Platform, Text, View, Image, TextInput, FlatList, ScrollView,TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { scale } from '../../../components/Scale';
import { GallaryList, ModalList, DeviceWidth,localImage } from '../../../config/global';
import Feather from 'react-native-vector-icons/Feather';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import AntDesign from 'react-native-vector-icons/AntDesign'
import { useIsFocused } from '@react-navigation/native';
import { Colors } from 'react-native-paper';
import { CustomLinearBotton, CustomLinearBotton1 } from '../../../components/Common/Button';
import { Header } from '../../../components/Header'
import { useSafeAreaFrame } from 'react-native-safe-area-context';
import { useTheme } from '../../../theme/Hooks'
import WalletConnectProvider from '@walletconnect/web3-provider';
import { useWalletConnect, withWalletConnect } from '@walletconnect/react-native-dapp';
import { ethers } from 'ethers';
const Wallet = props => {
  const isFocused = useIsFocused();
  const { NavigationTheme } = useTheme();
  const { colors } = NavigationTheme;
  const [isLoginScreen,setIsLoginScreen] = useState(false)
    const connector = useWalletConnect(); // valid
    const [connected, setconnected] = useState(false)
    const [walletAddress, SetWalletAddress] = useState('')
    useEffect(() => {
      if (connector  && connector.accounts && connector.accounts.length && connector.accounts[0]) {
        setconnected(true)
      }
    }, [isFocused]);
  
    const onConnect = async () => {
      connector.connect()
      const provider = new WalletConnectProvider({
        bridge: "https://bridge.walletconnect.org",
        rpc: { 1: "https://rpc.ankr.com/eth", 3: "https://ropsten.infura.io/v3/9aa3d95b3bc440fa88ea12eaa4456161" },
        infuraId: "8043bb2cf99347b1bfadfb233c5325c0",
        connector: connector,
        connected: true,
        qrcode: false,
      });
      await provider.enable();
      const ethers_provider = new ethers.providers.Web3Provider(provider);
      setWalletAddress(connector.accounts[0])
      setconnected(true)
    }
  useEffect(() => {

  }, []);
  
 

  return (
    <View >
      <Header
        navigation={props.navigation}
        name={'Home'}
      />
      <View style={{ margin: 10 }}>

      <View style={{
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: scale(50),
        marginBottom:scale(20)
      }}>
        <Image
          style={{
            width: 200,
            height: 200,
            borderRadius: 5,
            marginBottom: scale(20)
          }}
          source={localImage.server}
        />
        <Text style={{ fontSize: scale(16), fontWeight: '300', color:colors.text }}>By connecting your wallet, you agree to our Terms of Service and our Privacy Policy</Text>

      </View>
      <CustomLinearBotton
              title={'Connect Wallet'}
              onNavigate={() => onConnect()}

            />
     
      <Text style={{ fontSize: scale(16), fontWeight: '300', color:colors.text,textAlign:'center', marginTop: scale(20) }}>Learn more about wallets</Text>

    </View>
        
    </View>
  );
};

const styles = StyleSheet.create({
  pageContainer: {
    marginTop: Platform.OS === 'ios' ? scale(40) : 0
  },
});

function mapStateToProps(state) {
  return {


  };
}

export default connect(mapStateToProps, {

})(Wallet);
