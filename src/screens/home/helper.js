/* eslint-disable react-native/no-color-literals */
/* eslint-disable sort-keys */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react';
import { TouchableOpacity, StyleSheet, Platform, Text, View, Image, TextInput, FlatList } from 'react-native';
import { connect } from 'react-redux';
import { scale } from '../../components/Scale';
import { localImage, ModalList, DeviceWidth } from '../../config/global';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import AntDesignIcon from 'react-native-vector-icons/AntDesign'
import { useIsFocused } from '@react-navigation/native';
import { Header } from '../../components/Header'
import { useTheme } from '../../theme/Hooks'
import { Colors } from '../../components/Common/Style'
/* eslint-disable react-native/no-inline-styles */
function getPreviousDay(date = new Date()) {
  const previous = new Date(date.getTime());
  previous.setDate(date.getDate() - 1);

  return previous;
}
import { useCountdown } from '../../hooks/useCountdown';
const ShowCounter = ({ days, hours, minutes, seconds,islarge }) => {
  const timerDesig = (val) => {
    return <View style={{ flexDirection: 'row', backgroundColor: Colors.red00, padding:islarge?scale(10): scale(2), paddingLeft:islarge?scale(20): scale(6), paddingRight:islarge?scale(20): scale(6), borderRadius: islarge?scale(8):scale(2) }}>
      <Text style={{ color: Colors.whiteFF }}>{val + ''}</Text>
    </View>
  }
  return (
    <View style={{ flexDirection: 'row',  }}>
      {timerDesig(days)}
      <Text style={{ color: Colors.red00, fontSize: islarge?scale(26):scale(18), textAlign: 'center' }}>{':'}</Text>

      {timerDesig(hours)}
      <Text style={{ color: Colors.red00, fontSize:islarge?scale(26): scale(18), textAlign: 'center' }}>{':'}</Text>

      {timerDesig(minutes)}
      <Text style={{ color: Colors.red00, fontSize: islarge?scale(26):scale(18), textAlign: 'center' }}>{':'}</Text>

      {timerDesig(seconds)}
    </View>
  );
};
export const CountdownTimer = ({ targetDate,islarge=false }) => {
  const [days, hours, minutes, seconds] = useCountdown(targetDate);

  if (days + hours + minutes + seconds <= 0) {
    return <ShowCounter
      days={'00'}
      hours={'00'}
      minutes={'00'}
      seconds={'00'}
      islarge={islarge}
    />;
  } else {
    return (
      <ShowCounter
        days={days}
        hours={hours}
        minutes={minutes}
        seconds={seconds}
        islarge={islarge}

      />
    );
  }
};
export const ViewArtistInfo = props => {

  const { NavigationTheme } = useTheme();
  const { colors } = NavigationTheme;
  useEffect(() => {

  }, []);

  const { ArtistDetails } = props
  return (
    <View style={{}}>

      <View>
        <View style={{ flexDirection: 'row', padding: 5, borderRadius: 10, marginRight: scale(10) }}>
          <View style={{ flex: 0.4 }}>
            <Image source={ArtistDetails.image}
              resizeMode={'stretch'}
              style={{ width: scale(100), height: scale(100), borderRadius: 10 }}
            />

          </View >
          <View style={{ flex: 0.6, margig: scale(10), padding: 15 }}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <Text style={{ color: colors.text, fontSize: scale(16), fontWeight: '500', }}>{'20'}</Text>
              <Text style={{ color: colors.text, fontSize: scale(16), fontWeight: '500', }}>{'1.2M'}</Text>
              <Text style={{ color: colors.text, fontSize: scale(16), fontWeight: '500', }}>{'1.2K'}</Text>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
              <Text style={{ color: colors.text, fontSize: scale(16), fontWeight: '500', }}>{'NFT'}</Text>
              <Text style={{ color: colors.text, fontSize: scale(16), fontWeight: '500', }}>{'Followers'}</Text>
              <Text style={{ color: colors.text, fontSize: scale(16), fontWeight: '500', }}>{'Votes'}</Text>
            </View>

            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
              <TouchableOpacity style={{ backgroundColor: '#EF8911', padding: 10, borderRadius: 5 }} onPress={() => alert('Not Implemened yet')}>

                <Text style={{ color: colors.text, fontSize: scale(12), fontWeight: '500', }}>{'FOLLOW'}</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{ backgroundColor: '#9225FF', padding: 10, borderRadius: 5 }} onPress={() => alert('Not Implemened yet')}>
                <Text style={{ color: colors.text, fontSize: scale(12), fontWeight: '500', }}>{'VOTE'}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <Text style={{ color: colors.text, fontSize: scale(16), fontWeight: '700' }}>{'Aditi Musunur'}</Text>
      </View>
    </View>
  );
};
export const LiveNFTList = (props) => {
  const { NavigationTheme } = useTheme();
  const { colors } = NavigationTheme;
  const { itemDetails = {}, item = {} } = props;
  // const { itemDetails = {} } = item;
  const now = Date.now();
  // const t1 = (new Date(item.auctionEndTimestamp)).getTime()
  const yesterdayDate = getPreviousDay();
  const t1 = (new Date(yesterdayDate)).getTime();

  return (<View style={{ marginRight: scale(10), padding: 10, marginBottom: 30, backgroundColor: colors.card, borderRadius: scale(10), borderRadius: 5, borderWidth: 0.5, borderColor: '#EBE7E7' }}>
    <TouchableOpacity onPress={() => props.navigation.navigate('ViewLiveNFT', { item })}>
      <View style={{}}>




        <Image
          source={item.image}
          resizeMode="stretch"

          style={{ height: 150, width: DeviceWidth / 1.2, borderRadius: 10 }}
        />

        {/* {item.auctionEndTimestamp ? <View style={{
                  position: 'absolute',
                  right: 5,
                  top: 5,
              }}>
                  <CountdownTimer targetDate={item.auctionEndTimestamp} />
              </View> : null} */}
        <View style={{
          position: 'absolute',
          right: 5,
          top: 5,
        }}>
          <CountdownTimer targetDate={yesterdayDate} />
        </View>
      </View>

      <View style={{ flexDirection: 'row', backgroundColor: colors.card, paddingLeft: 15, paddingRight: 15, padding: 10 }}>
        <View style={{ flex: 0.7, }}>
          <Text style={{ fontSize: 14, fontWeight: 'bold', color: colors.text }}>{'MAD BOYS #3872349'}</Text>
          <Text style={{ fontSize: 12, color: colors.text }}>{'Current bid with Vijai Sritharan '}</Text>

        </View>
        <View style={{ flex: 0.3, color: colors.text }}>
          <Text style={{ fontSize: 12, color: colors.text }}>{'Highest Price'}</Text>
          <Text style={{ fontSize: scale(16), color: colors.text }}>{'₹225.0'}</Text>

        </View>
      </View>


    </TouchableOpacity>
  </View>
  );
};
export const SoldNFTList = (props) => {
  const { NavigationTheme } = useTheme();
  const { colors } = NavigationTheme;
  const { itemDetails = {}, item = {} } = props;
  // const { itemDetails = {} } = item;
  const now = Date.now();
  // const t1 = (new Date(item.auctionEndTimestamp)).getTime()
  const yesterdayDate = getPreviousDay();
  const t1 = (new Date(yesterdayDate)).getTime();

  return (<View style={{ marginRight: scale(10), padding: 10, marginBottom: 30, backgroundColor: colors.card, borderRadius: scale(10), borderRadius: 5, borderWidth: 0.5, borderColor: '#EBE7E7' }}>
    <TouchableOpacity onPress={() => props.navigation.navigate('ViewSoldNF', { item })}>
      <View style={{}}>




        <Image
          source={item.image}
          resizeMode="stretch"

          style={{ height: 150, width: DeviceWidth / 1.2, borderRadius: 10 }}
        />

        <View style={{
          position: 'absolute',
          right: 5,
          top: 5,
        }}>
          <View style={{ backgroundColor: colors.card, padding: 10, borderRadius: 5 }}>
            <Text style={{ fontSize: 12, color: colors.text }}>Fixed Price</Text>
          </View>
        </View>
      </View>

      <View style={{ flexDirection: 'row', backgroundColor: colors.card, paddingLeft: 15, paddingRight: 15, padding: 10 }}>
        <View style={{ flex: 0.7, }}>
          <Text style={{ fontSize: 14, fontWeight: 'bold', color: colors.text }}>{'MAD BOYS #3872349'}</Text>
          <Text style={{ fontSize: 12, color: colors.text }}>{'Current bid with Vijai Sritharan '}</Text>

        </View>
        <View style={{ flex: 0.3, color: colors.text }}>
          <Text style={{ fontSize: 12, color: colors.text }}>{'Highest Price'}</Text>
          <Text style={{ fontSize: scale(16), color: colors.text }}>{'₹225.0'}</Text>

        </View>
      </View>


    </TouchableOpacity>
  </View>
  );
};

export const NFTImgeView = (props) => {
  const { NavigationTheme } = useTheme();
  const { colors } = NavigationTheme;
  const { itemDetails = {}, item = {} } = props;


  return (<View style={{padding: 10, marginBottom: 30, }}>
    <View>

      <Image
        source={item.image}
        resizeMode="stretch"

        style={{ height: 350, width: DeviceWidth / 1.1, borderRadius: 10 }}
      />

      <View style={{
        position: 'absolute',
        right: 5,
        top: 5,
      }}>
        <View style={{ backgroundColor: colors.card, padding: 10, borderRadius: 5 }}>
          <Text style={{ fontSize: 12, color: colors.text }}>Fixed Price</Text>
        </View>
      </View>
    </View>

    <View style={{ backgroundColor: colors.card1, paddingLeft: 15, paddingRight: 15, padding: 10, borderRadius: scale(10) }}>
      <Text style={{ fontSize: scale(18), fontWeight: 'bold', color: colors.text }}>{'Silent Color'}</Text>

      <View style={{ backgroundColor: colors.card, width: DeviceWidth / 2, borderRadius: 10, alignContent: 'center', padding: 7, paddingLeft: -7, marginTop: 10 }}>
        <Text style={{ fontSize: scale(18), fontWeight: 'bold', color: colors.text, textAlign: 'center' }}>{'Creator Barasthi'}</Text>
      </View>
      <View>
        <Text style={{ fontSize: scale(14), color: colors.text }}>{'Together with my design team, we designed this futuristic Cyberyacht concept artwork. We wanted to create something that has not been created yet, so we started to collect ideas of how we imagine the Cyberyacht could look like in the future. '}</Text>

      </View>
    </View>


  </View>
  );
};
export const NFTSoldPrice = (props) => {
  const { NavigationTheme } = useTheme();
  const { colors } = NavigationTheme;


  return (<View style={{ backgroundColor: colors.card, padding: 10, margin: scale(10), borderRadius: scale(10) }}>

    <View style={{ flexDirection: 'row', backgroundColor: colors.card, padding: 10 }}>
      <View style={{ flex: 0.5, }}>
        <Text style={{ fontSize: scale(14), color: colors.text }}>{'Sold for'}</Text>


      </View>
      <View style={{ flex: 0.5, color: colors.text }}>
        <Text style={{ fontSize: scale(16), color: colors.text }}>{'₹225.0'}</Text>

      </View>
    </View>
    <View style={{ flexDirection: 'row', backgroundColor: colors.card, padding: 10 }}>
      <View style={{ flex: 0.3, }}>
        <Text style={{ fontSize: scale(14), color: colors.text }}>{'Owner by'}</Text>


      </View>
      <View style={{ flex: 0.4, color: colors.text }}>
        <Text style={{ fontSize: scale(16), color: colors.text }}>{'@david'}</Text>

      </View>
    </View>
  </View>
  );
};
export const NFTActivity = (props) => {
  const { NavigationTheme } = useTheme();
  const { colors } = NavigationTheme;
  const eachActivity = (item) => {
    return <View style={{ flexDirection: 'row', padding: 10, borderRadius: 10, marginRight: scale(10), }}>
      <View style={{ flex: 0.2 }}>
        <Image source={item.image}
          resizeMode={'stretch'}
          style={{ width: scale(30), height: scale(30), borderRadius: 10 }}
        />

      </View>
      <View style={{ flex: 0.8, flexDirection: 'row', marginLeft: scale(10) }}>
        <View style={{ flex: 9, justifyContent: 'center' }}>
          <Text style={{ color: colors.text, fontSize: scale(14), fontWeight: '700', }}>{'Bid place by @Davee'}</Text>
          <Text style={{ color: colors.text, fontSize: scale(12), fontWeight: '400', }}>{'June 04, 2021 at 12:00am'}</Text>
          <Text style={{ color: colors.text, fontSize: scale(14), fontWeight: '700', }}>{'Sold for ₹ 2749.00'}</Text>

        </View>






      </View>
      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
        <TouchableOpacity onPress={()=>alert('Not implemented yet')}>
        <FontAwesome name='share-square-o' size={25} color={colors.text} />

        </TouchableOpacity>

        <View>
        </View>
      </View>
    </View>
  }

  return (<View style={{ backgroundColor: colors.card, padding: 10, margin: scale(10) }}>
    <Text style={{ color: colors.text, fontSize: scale(14), fontWeight: '700',marginBottom:scale(20) }}>{'Activity'}</Text>

    <FlatList
      // onEndReachedThreshold={0.7}
      horizontal={false}
      keyExtractor={(item, index) => index.toString()}
      data={ModalList}
      renderItem={({ item }) => eachActivity(item)}

    />
  </View>
  );
};
export const NFTBidPrice = (props) => {
  const { NavigationTheme } = useTheme();
  const { colors } = NavigationTheme;
  const yesterdayDate = getPreviousDay();


  return (<View style={{ backgroundColor: colors.card, padding: 10, margin: scale(10), borderRadius: scale(10) }}>
    <View style={{margin:5}}>


        <Text style={{ fontSize: scale(14), color: colors.text }}>{'Current Bid'}</Text>


      <View style={{ flexDirection:'row',marginTop:scale(10) }}>
        <Text style={{ fontSize: scale(16), color: colors.text }}>{'0.50 ETH'}</Text>
        <Text style={{ fontSize: scale(16), color: colors.text }}>{'   $2,683.73'}</Text>

    </View>
    <View style={{marginTop:scale(10)}}>
    <Text style={{ fontSize: scale(14), color: colors.text,marginBottom:scale(15) }}>{'Auction ending in'}</Text>

    <CountdownTimer targetDate={yesterdayDate} islarge={true}/>
    </View>
    </View>

   
  </View>
  );
};


export const TableHeaderText = (props) => {
  const { NavigationTheme } = useTheme();
  const { colors } = NavigationTheme;
  const { first, second, third } = props
  return <View style={{ flexDirection: 'row', padding: scale(15) }}>
      <View style={{ flex: 0.1, justifyContent: 'center', alignItems: 'center' }}>
          <Text  style={{ color: colors.text, fontSize: scale(14), fontWeight: '700', }}>{first}</Text>
      </View>
      <View style={{ flex: 0.7, justifyContent: 'center', alignItems: 'center' }}>
          <Text  style={{ color: colors.text, fontSize: scale(14), fontWeight: '700', }}>{second}</Text>
      </View>
      <View style={{ flex: 0.2, justifyContent: 'center', alignItems: 'center' }}>
          <Text style={{ color: colors.text, fontSize: scale(14), fontWeight: '700', }}>{third}</Text>

      </View>
     

  </View>
}
export const TableLeaderBoard = (props) => {
  const { NavigationTheme } = useTheme();
  const { colors } = NavigationTheme;
  const { item,index} = props
  return <View style={{ flexDirection: 'row', padding: scale(15) }}>
      <View style={{ flex: 0.1, justifyContent: 'center', alignItems: 'center' }}>
          <Text  style={{ color: colors.text, fontSize: scale(14), fontWeight: '700',}}>{index+1+''}</Text>
      </View>
      <View style={{ flex: 0.7, justifyContent: 'center', alignItems: 'center' }}>
          <Text  style={{ color: colors.text, fontSize: scale(14), fontWeight: '700', }}>{'Sudhir Kumar'}</Text>
      </View>
      <View style={{ flex: 0.2, justifyContent: 'center', alignItems: 'center' }}>
      <View style={{ padding:10,borderWidth:1,borderColor:'#1098FC',borderRadius:5}}>
          <Text style={{ color: colors.text, fontSize: scale(14), fontWeight: '700', }}>{'2.5 M'}</Text>
          </View>

      </View>
     

  </View>
}