/* eslint-disable react-native/no-color-literals */
/* eslint-disable sort-keys */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react';
import { TouchableOpacity, StyleSheet, Platform, Text, View, Image, TextInput, FlatList } from 'react-native';
import { connect } from 'react-redux';
import { scale } from '../../components/Scale';
import { localImage, ModalList, DeviceWidth } from '../../config/global';
import Feather from 'react-native-vector-icons/Feather';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import AntDesign from 'react-native-vector-icons/AntDesign'
import { useIsFocused } from '@react-navigation/native';
import { Colors } from 'react-native-paper';
import { Header } from '../../components/Header'
import { useTheme } from '../../theme/Hooks'
import { ScrollView } from 'react-native-gesture-handler';
import { CustomLinearBotton, CustomLinearBotton1, CustomLinearBottonCircle } from '../../components/Common/Button';

const HomeScreen = props => {
  const isFocused = useIsFocused();
  const { NavigationTheme } = useTheme();
  const { colors } = NavigationTheme;
  const [searchText, setSearchText] = useState('')
  useEffect(() => {

  }, []);
  const eachArtist = (item) => {
    return <TouchableOpacity style={{ padding: 5, borderRadius: 10, marginRight: scale(5) }} onPress={() => props.navigation.navigate('ViewArtist', { ArtistDetails: item })}>
      <View>
        <Image source={item.image}
          resizeMode={'stretch'}
          style={{ width: scale(150), height: scale(150), borderTopLeftRadius: 10, borderTopRightRadius: 10 }}
        />

      </View>
      <View style={{ flexDirection: 'row', borderBottomEndRadius: 10, borderBottomLeftRadius: 10, backgroundColor: '#0A64E4', padding: 10, width: scale(150), }}>
        <Text style={{ color: colors.text, fontSize: scale(12), fontWeight: '500', flex: 0.9 }}>{'Aditi Musunur'}</Text>
        <Text style={{ color: '#39F9E2', fontSize: scale(12), fontWeight: '500', }}>{'1.2M'}</Text>

      </View>
    </TouchableOpacity>
  }
  const eachArtist1 = (item) => {
    return <TouchableOpacity style={{ padding: 5, borderRadius: 10, marginRight: scale(10) }} onPress={() => props.navigation.navigate('ViewArtist', { ArtistDetails: item })}>
      <View>
        <Image source={item.image}
          resizeMode={'stretch'}
          style={{ width: scale(160), height: scale(150), borderTopLeftRadius: 10, borderTopRightRadius: 10 }}
        />

      </View>
      <View style={{ borderBottomEndRadius: 10, borderBottomLeftRadius: 10, backgroundColor: '#0A64E4', padding: 10, }}>
        <Text style={{ color: colors.text, fontSize: scale(12), fontWeight: '500', flex: 0.9 }}>{'Aditi Musunur'}</Text>

      </View>
    </TouchableOpacity>
  }
  const TopArtist = (item) => {
    return <View>
      <View style={{ marginTop: scale(15), flexDirection: 'row', alignItems: 'center' }}>

        <Text style={{ color: colors.text, fontSize: scale(22), fontWeight: '700', flex: 1 }}>{'Top Artist'}</Text>
        <TouchableOpacity
          onPress={() => alert('Not Implemented yet')}
        >
          <AntDesign name="arrowright" size={40} color={colors.text} />
        </TouchableOpacity>
      </View>
      <FlatList
        // onEndReachedThreshold={0.7}
        horizontal={false}
        numColumns={2}
        keyExtractor={(item, index) => index.toString()}
        data={ModalList}
        renderItem={({ item }) => eachArtist(item)}

      />

    </View>
  }
  const MostFollowers = (item) => {
    return <View>
      <View style={{ marginTop: scale(15), flexDirection: 'row', alignItems: 'center' }}>

        <Text style={{ color: colors.text, fontSize: scale(22), fontWeight: '700', flex: 1 }}>{'HIGH FLOWIING'}</Text>
        <TouchableOpacity
          onPress={() => alert('Not Implemented yet')}
        >
          <AntDesign name="arrowright" size={40} color={colors.text} />
        </TouchableOpacity>
      </View>
      <FlatList
        // onEndReachedThreshold={0.7}
        horizontal={true}
        keyExtractor={(item, index) => index.toString()}
        data={ModalList}
        renderItem={({ item }) => eachArtist1(item)}

      />

    </View>
  }
  const HomeTopCommpenent = (item) => {
    return <View>
      <Text style={{ color: colors.text, fontSize: scale(14), fontWeight: '700', textAlign: 'center' }}>{'Discover, collect, and sell'}</Text>
      <Text style={{ color: colors.text, fontSize: scale(28), fontWeight: '700', textAlign: 'center', marginTop: scale(15) }}>{'Your Digital Art'}</Text>
      <View style={{ flexDirection: 'row', borderColor: colors.text, borderWidth: 1, marginTop: scale(10), borderRadius: scale(10), alignItems: 'center', backgroundColor: colors.card }}>
        <TouchableOpacity
          onPress={() => alert('Not Implemented yet')}
          style={{ marginLeft: 10 }}
        >
          <Feather name="search" size={25} color={colors.text} />
        </TouchableOpacity>

        <TextInput
          style={{ padding: scale(7), color: colors.text, fontSize: scale(20), flex: 1 }}
          value={searchText}
          placeholder={'Search items, collections, and accounts'}
          placeholderTextColor={colors.text}
          onChangeText={(text) => setSearchText(text)}
        />
        <TouchableOpacity
          style={{ marginRight: 10 }}
          onPress={() => alert('Not Implemented yet')}
        >
          <MaterialIcons name="keyboard-voice" size={25} color={colors.text} />
        </TouchableOpacity>
      </View>

    </View>
  }
  const LeaderBordCommpenent = (item) => {
    return <TouchableOpacity onPress={()=>{props.navigation.navigate('Leaderboard')}}>
      <View style={{ marginTop: scale(15), flexDirection: 'row', alignItems: 'center' }}>

        <Text style={{ color: colors.text, fontSize: scale(22), fontWeight: '700', flex: 1 }}>{'LEADERBOARD'}</Text>
        <TouchableOpacity
          onPress={() => alert('Not Implemented yet')}
        >
          <AntDesign name="arrowright" size={40} color={colors.text} />
        </TouchableOpacity>
      </View>
      <View style={{ backgroundColor: colors.card, borderTopRightRadius: 30, marginTop: scale(10) }}>
        <Image source={localImage.tournment}
          resizeMode={'stretch'}
          style={{ width: DeviceWidth / 1.1, height: scale(150), borderTopLeftRadius: 10, borderTopRightRadius: 10 }}
        />
        <View style={{ padding: 10, marginTop: scale(20), alignItems: 'center', marginBottom: scale(20) }}>
          <Text style={{ color: colors.text, fontSize: scale(16), fontWeight: '700', }}>{'COMPETITIONS'}</Text>
          <Text style={{ color: colors.text, fontSize: scale(12), fontWeight: '400', marginTop: scale(10) }}>{'Check the competion and support your favouriate Artist'}</Text>
        </View>

      </View>

    </TouchableOpacity>
  }
  const BottomCommpenent = () => {
    return <View>
      <View style={{ marginTop: scale(15), flexDirection: 'row', alignItems: 'center', marginBottom: scale(15), }}>

        <Text style={{ color: colors.text, fontSize: scale(22), fontWeight: '700', flex: 1 }}>{'HOW IT WORKS?'}</Text>
        <TouchableOpacity
          onPress={() => alert('Not Implemented yet')}
        >
          <AntDesign name="arrowright" size={40} color={colors.text} />
        </TouchableOpacity>
      </View>
      <Text style={{ color: colors.text, fontSize: scale(16), fontWeight: '300', }}>{'Easy to buy & sell your Digital Art with 3 step'}</Text>
      <View style={{ marginTop: scale(15), flexDirection: 'row', justifyContent: 'space-between', marginBottom: scale(15), }}>
        <View style={{alignItems:'center'}}>
          <CustomLinearBottonCircle
            title={'1'}
            onNavigate={() => alert('Not Implemnted')}
          />
          <Text style={{ color: colors.text, fontSize: scale(12), fontWeight: '300',marginTop:scale(10) }}>{'Follow Artist'}</Text>

        </View>

        <AntDesign name="arrowright" size={40} color={colors.text} />

        <View style={{alignItems:'center'}}>
                    <CustomLinearBottonCircle
            title={'2'}
            onNavigate={() => alert('Not Implemnted')}
          />
          <Text style={{ color: colors.text, fontSize: scale(12), fontWeight: '300', marginTop:scale(10)}}>{'Buy their NFT’s'}</Text>

        </View>
        <AntDesign name="arrowright" size={40} color={colors.text} />

        <View style={{alignItems:'center'}}>
          <CustomLinearBottonCircle
            title={'3'}
            onNavigate={() => alert('Not Implemnted')}
          />
          <Text style={{ color: colors.text, fontSize: scale(12), fontWeight: '300',marginTop:scale(10) }}>{'Resale'}</Text>

        </View>


      </View>

      <CustomLinearBotton
        title={'Discover more'}
        onNavigate={() => props.navigation.navigate('Community', { screen: 'Discover' })}
      />
      <CustomLinearBotton1
        title={'FAQ’s & Support'}
        onNavigate={() => props.navigation.navigate('Community', { screen: 'FaqSupport' })}
      />

    </View>
  }
  const HomeScreen=(item)=>{
    return <View>
      {HomeTopCommpenent()}
        {TopArtist()}
        {MostFollowers()}
        {LeaderBordCommpenent()}
        {BottomCommpenent()}
    </View>
  }
  return (
    <View >
      <Header
        navigation={props.navigation}
        name={'Home'}
      />
      <View style={{ margin: scale(15) }}>
      <FlatList
            onEndReachedThreshold={0.7}
            style={{ width: '100%' }}
            keyExtractor={(item, index) => index.toString()}
            data={['test']}
            renderItem={({ item, index }) => HomeScreen(item, index)}
            ListFooterComponent={<View style={{ height: DeviceWidth }} />}

        />

      

      </View>






    </View>
  );
};

const styles = StyleSheet.create({
  pageContainer: {
    marginTop: Platform.OS === 'ios' ? scale(40) : 0
  },
});

// export default HomeScreen;
function mapStateToProps(state) {
  return {


  };
}

export default connect(mapStateToProps, {

})(HomeScreen);
