import React, { useEffect, useState } from 'react';
import { TouchableOpacity, StyleSheet, Platform, Text, View, Image, TextInput, FlatList } from 'react-native';
import { moderateScale, scale, verticalScale } from '../../../components/Scale';
import { SelectionSelect1 } from '../../../components/SelectionSelect'
import { useTheme } from '../../../theme/Hooks'
import { imageslist } from '../../../config/global'

import { NavigationContainer, useIsFocused } from '@react-navigation/native';
const ImageList = (props) => {
  const { NavigationTheme } = useTheme();
  const { colors } = NavigationTheme;
  const eachNftImge = (item) => {
    return <TouchableOpacity style={{ padding: 10 }} onPress={() => props.navigation.navigate('ArtistNFTList', { item })}>
    <Image
      source={item.image}
      resizeMode="stretch"

      style={{ height: 100, width: 120, borderRadius: 10 }} />

    </TouchableOpacity>
  }
  return (
    <View >
      <FlatList
        onEndReachedThreshold={0.7}
        horizontal={false}
        numColumns={3}
        keyExtractor={(item, index) => index.toString()}
        data={imageslist}
        renderItem={({ item }) => eachNftImge(item)}
      />
    </View>
  );
};
export default ImageList;