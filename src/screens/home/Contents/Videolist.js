import React, { useEffect, useState,useRef } from 'react';
import { TouchableOpacity, StyleSheet, Platform, Text, View, Image, TextInput, FlatList,Button } from 'react-native';
import { moderateScale, scale, verticalScale } from '../../../components/Scale';
import {SelectionSelect1} from '../../../components/SelectionSelect'
import { useTheme } from '../../../theme/Hooks'
import {imageslist} from '../../../config/global'
import { Video, AVPlaybackStatus } from 'expo-av';
import Expo from 'expo';

import { NavigationContainer, useIsFocused } from '@react-navigation/native';
const VideoList = (props) => {
  const video = useRef(null);
  const [status, setStatus] = useState({});
  const { NavigationTheme } = useTheme();
  const { colors } = NavigationTheme;
  const eachNftImge = (item) => {
    return <View style={{ backgroundColor: 'black',padding:5,borderRadius:5}}>
      <Video
        ref={video}
        source={{
          uri: 'https://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4',
        }}
        useNativeControls
        resizeMode="cover"
        isLooping
        style={{ width: 110, height: 120 }}
        onPlaybackStatusUpdate={status => setStatus(() => status)}
      />

    </View>
  }
  return (
    <View >
     <FlatList
        onEndReachedThreshold={0.7}
        horizontal={false}
        numColumns={3}
        keyExtractor={(item, index) => index.toString()}
        data={imageslist}
        renderItem={({ item }) => eachNftImge(item)}
      />
    </View>
  );
};
export default VideoList;