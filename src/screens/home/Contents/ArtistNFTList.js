/* eslint-disable react-native/no-color-literals */
/* eslint-disable sort-keys */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react';
import { useColorScheme, StyleSheet, Platform, Text, View, Image, TouchableOpacity, FlatList, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import { scale } from '../../../components/Scale';
import { ModalList1, GallaryList, DeviceWidth } from '../../../config/global';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import AntDesign from 'react-native-vector-icons/AntDesign'
import { useIsFocused } from '@react-navigation/native';
import { HeaderWithBack } from '../../../components/Header'
import { useTheme } from '../../../theme/Hooks'

const ArtistNFTList = props => {
  const isFocused = useIsFocused();
  const { NavigationTheme } = useTheme();
  const { colors } = NavigationTheme;
  useEffect(() => {

  }, []);

  const eachSaleList = (item) => {
    return <View style={{ borderRadius: 10, padding: 10, borderBottomWidth: 0.5, borderBottomColor: '#363636' }}>

      <View style={{ flex: 1, flexDirection: 'row' }}>
        <View style={{ flex: 1 }}>
          <Image source={item.image}
            resizeMode="stretch"
            style={{ width: scale(20), height: scale(20), borderRadius: 15 }}
          />
 
        </View>
        <View style={{ flex: 9 }}>

          <Text style={{ color: colors.text, fontSize: scale(16), fontWeight: '400', }}>{'Aditi Musunur'}</Text>
        </View>
        <TouchableOpacity style={{}} onPress={() => alert('not implemented yet')}>

          <MaterialCommunityIcons name='dots-vertical' color={colors.text} size={25} />
        </TouchableOpacity>
      </View>
      <View style={{ flex: 1, marginTop: 15 }}>
        <Image source={item.image}
          resizeMode={'stretch'}
          style={{ width: DeviceWidth / 1.1, height: scale(200), }}
        />
        <View style={{ backgroundColor: '#84FFE9', width: DeviceWidth / 1.1, }}>
          <Text style={{ fontSize: scale(12), fontWeight: '400', padding: 10, }}>{'Buy the Nft'}</Text>

        </View>
        <View style={{ flexDirection: 'row', marginTop: 10, alignItems: 'center' }}>
          <View style={{ flexDirection: 'row', flex: 0.7, alignItems: 'center' }}>
            <AntDesign name='hearto' color={'red'} size={25} />
            <Text style={{ color: colors.text, fontSize: scale(12), fontWeight: '400', padding: 10, }}>{'28,78,878'}</Text>
          </View>
          <View style={{ flexDirection: 'row', flex: 0.3 }}>
            <Text style={{ color: colors.text, fontSize: scale(12), fontWeight: '400', padding: 10, }}>{'21 hours Ago'}</Text>

          </View>


        </View>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <Text style={{ color: colors.text, fontSize: scale(12), fontWeight: '900', }}>{'Barasati Sandipa'}</Text>
          <Text style={{ color: colors.text, fontSize: scale(12), fontWeight: '400', }}>{' Tum Hi Ho Song by Arijit Singh...'}</Text>

        </View>
      </View>
    </View>
  }

  return (
    <ScrollView >
      <HeaderWithBack
        navigation={props.navigation}
        name={'NFT Details'}
      />
      <View style={{ margin: 10 }}>



          <FlatList
            // onEndReachedThreshold={0.7}
            horizontal={false}
            keyExtractor={(item, index) => index.toString()}
            data={GallaryList}
            renderItem={({ item }) => eachSaleList(item)}

          />
        </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  pageContainer: {
    marginTop: Platform.OS === 'ios' ? scale(40) : 0
  },
});

// export default ArtistNFTList;
function mapStateToProps(state) {
  return {


  };
}

export default connect(mapStateToProps, {

})(ArtistNFTList);
