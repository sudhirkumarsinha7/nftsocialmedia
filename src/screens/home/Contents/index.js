import React, { useEffect, useState } from 'react';
import { TouchableOpacity, StyleSheet, Platform, Text, View, Image, TextInput, FlatList } from 'react-native';
import { moderateScale, scale, verticalScale } from '../../../components/Scale';
import {SelectionSelectTabIcon} from '../../../components/SelectionSelect'
import { useTheme } from '../../../theme/Hooks'
import ImageList from './Imagelist'
import VideoList from './Videolist'
import AudioList from './Audiolist'

let radioItems = [
  {
    label: 'Audio',
    selected: true,
  },
  {
    label: 'Image',
    selected: false,
  },
  {
    label: 'Video',
    selected: false,
  },
]
const Contents = (props) => {
  const { NavigationTheme } = useTheme();
  const { colors } = NavigationTheme;
  const [selectedTab, setTab] = useState('Audio');
  const changeActiveRadioButton = (index) => {
    radioItems.map(item => {
      item.selected = false;
    });

    radioItems[index].selected = true;
    setTab(radioItems[index].label)
  }
  return (
    <View style={{}}>

<View style={{ flexDirection: 'row', marginBottom: verticalScale(10), elevation: 5 }}>
        {radioItems.map((item, key) => (
          <SelectionSelectTabIcon
            key={key}
            button={item}
            onClick={() => changeActiveRadioButton(key)}
            selectedBackgraoundColor={'#84FFE9'}
            unSelectedTextColor={'green'}
          />
        ))}
      </View>
      {selectedTab === 'Audio' ? <AudioList navigation={props.navigation}/> : selectedTab === 'Video'?<VideoList navigation={props.navigation}/> :selectedTab === 'Image'? <ImageList navigation={props.navigation}/>:null}

    </View>
  );
};
export default Contents;