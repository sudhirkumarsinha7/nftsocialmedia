import React, { useEffect, useState } from 'react';
import { TouchableOpacity, StyleSheet, Platform, Text, View, Image, TextInput, FlatList } from 'react-native';
import { moderateScale, scale, verticalScale } from '../../../components/Scale';
import {SelectionSelect1} from '../../../components/SelectionSelect'
import { useTheme } from '../../../theme/Hooks'
import LiveList from './Live'
import SoldList from './Sold'
import { NavigationContainer, useIsFocused } from '@react-navigation/native';

let radioItems = [
  {
    label: 'Live Now',
    selected: true,
  },
  {
    label: 'Already Sold',
    selected: false,
  },
]
const NFTStore = (props) => {
  const isFocused = useIsFocused();

  const { NavigationTheme } = useTheme();
  const { colors } = NavigationTheme;
  const [selectedTab, setTab] = useState('Live Now');
  useEffect(() => {
    setTab('Live Now')
    changeActiveRadioButton(0)
  }, [isFocused]);
  const changeActiveRadioButton = (index) => {
    radioItems.map(item => {
      item.selected = false;
    });

    radioItems[index].selected = true;
    setTab(radioItems[index].label)
  }
  return (
    <View style={{}}>
      <View style={{ flexDirection: 'row', marginBottom: verticalScale(10), elevation: 5 }}>
        {radioItems.map((item, key) => (
          <SelectionSelect1
            key={key}
            button={item}
            onClick={() => changeActiveRadioButton(key)}
            selectedBackgraoundColor={'#84FFE9'}
          />
        ))}
      </View>
      {selectedTab === 'Already Sold'?<SoldList  navigation={props.navigation}/>:selectedTab === 'Live Now' ? <LiveList  navigation={props.navigation}/> : null}

    </View>
  );
};
export default NFTStore;