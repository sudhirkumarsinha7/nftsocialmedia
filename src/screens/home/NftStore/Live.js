import * as React from 'react';
import {View, Text,FlatList} from 'react-native';
import { PropertisInfo } from '../../../components/Explore/helper';
import {imageslist} from '../../../config/global'
import {LiveNFTList} from '../helper'
const LiveNFTScreen = (props) => {
  const eachNft =(item)=>{
    return<View>
        <LiveNFTList item={item} navigation={props.navigation}/>
    </View>
  }
  return (
    <View style={{}}>
        <FlatList
          onEndReachedThreshold={0.7}
          keyExtractor={(item, index) => index.toString()}
          data={imageslist}
          renderItem={({ item }) => eachNft(item)}
        />
    </View>
  );
};
export default LiveNFTScreen;