import * as React from 'react';
import {View, Text,ScrollView} from 'react-native';
import { HeaderWithBack } from '../../../components/Header'
import {NFTImgeView,NFTSoldPrice,NFTActivity,NFTBidPrice} from '../helper'

const ViewLiveNFT = (props) => {
    const nftDetails = props.route.params.item;

  return (
    <ScrollView >
        <HeaderWithBack
        navigation={props.navigation}
        name={'NFT Details'}
      />
      <NFTImgeView 
      item={nftDetails}
      />
           <NFTBidPrice  item={nftDetails}/>

      <NFTActivity 
        item={nftDetails}/>
    </ScrollView>

  );
};
export default ViewLiveNFT;