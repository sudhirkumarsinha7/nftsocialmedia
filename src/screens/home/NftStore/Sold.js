

import * as React from 'react';
import {View, Text,FlatList} from 'react-native';
import {imageslist} from '../../../config/global'
import {SoldNFTList} from '../helper'
const SoldNFTSCreen = (props) => {
  const eachNft =(item)=>{
    return<View>
        <SoldNFTList item={item}  navigation={props.navigation}/>
    </View>
  }
  return (
    <View style={{}}>
        <FlatList
          onEndReachedThreshold={0.7}
          keyExtractor={(item, index) => index.toString()}
          data={imageslist}
          renderItem={({ item }) => eachNft(item)}
        />
    </View>
  );
};
export default SoldNFTSCreen;