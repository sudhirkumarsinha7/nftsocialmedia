/* eslint-disable react-native/no-color-literals */
/* eslint-disable sort-keys */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react';
import { TouchableOpacity, StyleSheet, Platform, Text, View, Image, TextInput, FlatList } from 'react-native';
import { connect } from 'react-redux';
import { localImage, ModalList, DeviceWidth } from '../../config/global';
import Feather from 'react-native-vector-icons/Feather';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import AntDesign from 'react-native-vector-icons/AntDesign'
import { useIsFocused } from '@react-navigation/native';
import { Header } from '../../components/Header'
import { useTheme } from '../../theme/Hooks'
// import { Tab, TabView } from '@rneui/base';
import { moderateScale, scale, verticalScale } from '../../components/Scale';

import { ImageComponent, VideoComponent } from '../../components/Common/heper'
import {ViewArtistInfo} from './helper'
import NftStore from './NftStore'
import Contents from './Contents'

import {SelectionSelectTab} from '../../components/SelectionSelect'

let radioItems = [
  {
    label: 'CONTENTS',
    selected: true,
  },
  {
    label: 'NFT STORE',
    selected: false,
  },
]
const ViewArtist = props => {
  const [index, setIndex] = React.useState(0);

  const isFocused = useIsFocused();
  const { NavigationTheme } = useTheme();
  const { colors } = NavigationTheme;
  const [selectedTab, setTab] = useState('CONTENTS');
  const [selectedTab1, setTab1] = useState('BID');

  const changeActiveRadioButton = (index) => {
    radioItems.map(item => {
      item.selected = false;
    });

    radioItems[index].selected = true;
    setTab(radioItems[index].label)
  }
  useEffect(() => {

  }, []);

  const { ArtistDetails } = props.route.params
  return (
    <View style={{}}>
      <Header
        navigation={props.navigation}
        name={'Home'}
      />
      <View style={{ margin: 10 }}>
        <ViewArtistInfo ArtistDetails ={ArtistDetails}/>
        <View style={{ flexDirection: 'row', marginBottom: verticalScale(10), elevation: 5 }}>
        {radioItems.map((item, key) => (
          <SelectionSelectTab
            key={key}
            button={item}
            onClick={() => changeActiveRadioButton(key)}
            selectedBackgraoundColor={'#84FFE9'}
            unSelectedTextColor={'green'}
          />
        ))}
      </View>
      {selectedTab === 'CONTENTS' ? <Contents  navigation={props.navigation}/> : <NftStore  navigation={props.navigation}/>}

      </View>

    </View>
  );
};

const styles = StyleSheet.create({
  pageContainer: {
    marginTop: Platform.OS === 'ios' ? scale(40) : 0
  },
});

function mapStateToProps(state) {
  return {


  };
}

export default connect(mapStateToProps, {

})(ViewArtist);
