import * as React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Home from './index';
import ViewArtist from './ViewArtist';
import ViewLiveNFT from './NftStore/ViewLiveNFT';
import ViewSoldNF from './NftStore/ViewSoldNF';
import ArtistNFTList from './Contents/ArtistNFTList';

import Leaderboard from './Leaderboard';


import Notification from './Notification';
import Wallet from './Notification/wallet';
const Stack = createNativeStackNavigator();
const HomeStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Home"
        component={Home}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="ViewArtist"
        component={ViewArtist}
        options={{
          headerShown: false,
        }}
      />
       <Stack.Screen
        name="Leaderboard"
        component={Leaderboard}
        options={{
          headerShown: false,
        }}
      />
      
       <Stack.Screen
        name="ViewLiveNFT"
        component={ViewLiveNFT}
        options={{
          headerShown: false,
        }}
      />
       <Stack.Screen
        name="ViewSoldNF"
        component={ViewSoldNF}
        options={{
          headerShown: false,
        }}
      />
       <Stack.Screen
        name="ArtistNFTList"
        component={ArtistNFTList}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="Notification"
        component={Notification}
        options={{
          headerShown: false,
        }}
      />
       <Stack.Screen
        name="Wallet"
        component={Wallet}
        options={{
          headerShown: false,
        }}
      />
    </Stack.Navigator>
  );
};
export default HomeStack;
