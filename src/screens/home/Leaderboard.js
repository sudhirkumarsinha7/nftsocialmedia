import * as React from 'react';
import {View, Text,ScrollView,FlatList} from 'react-native';
import { HeaderWithBack } from '../../components/Header'
import {TableHeaderText,TableLeaderBoard} from './helper'
import { useTheme } from '../../theme/Hooks'
import { scale } from '../../components/Scale';
import { localImage, ModalList, DeviceWidth } from '../../config/global';

const ViewSoldNF = (props) => {
  const { NavigationTheme } = useTheme();
  const { colors } = NavigationTheme
  const eachleader =(item,index)=>{
    return (<TableLeaderBoard 
      item={item}
      index={index}
       first ={'#'} 
    second ={'Artist'}
    third={'Votes'}/>)
  }
  return (
    <ScrollView >
        <HeaderWithBack
        navigation={props.navigation}
        name={'LEADERBOARD'}
      />
      <View style={{margin:15}}>
        <View style={{alignItems:'center',}}>

          <Text style={{ color: colors.text, fontSize: scale(22), fontWeight: '700',marginBottom:scale(15) }}>{'LEADERBOARD'}</Text>
          <Text style={{ color: colors.text, fontSize: scale(18), fontWeight: '700',  }}>{'TOP ARTIST'}</Text>
      </View>
      <View style={{backgroundColor:colors.card,borderRadius:scale(15),marginTop:scale(20)}}>
     <TableHeaderText
     first ={'#'} 
     second ={'Artist'}
     third={'Votes'}
     />
     <FlatList
        // onEndReachedThreshold={0.7}
        keyExtractor={(item, index) => index.toString()}
        data={ModalList}
        renderItem={({ item,index }) => eachleader(item,index)}

      />
 </View>

      </View>
     
    </ScrollView>

  );
};
export default ViewSoldNF;