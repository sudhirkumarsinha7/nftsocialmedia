/* eslint-disable react-native/no-color-literals */
/* eslint-disable sort-keys */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react';
import { TouchableOpacity, StyleSheet, Platform, Text, View, Image, TextInput, FlatList } from 'react-native';
import { connect } from 'react-redux';
import { scale } from '../../components/Scale';
import { localImage, ModalList, DeviceWidth } from '../../config/global';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import AntDesignIcon from 'react-native-vector-icons/AntDesign'
import { useIsFocused } from '@react-navigation/native';
import { Header } from '../../components/Header'
import { useTheme } from '../../theme/Hooks'
import { Colors } from '../../components/Common/Style'
/* eslint-disable react-native/no-inline-styles */

export const LiveNFTList = (props) => {
    const { NavigationTheme } = useTheme();
    const { colors } = NavigationTheme;
    const { itemDetails = {}, item = {} } = props;
    // const { itemDetails = {} } = item;
    const now = Date.now();
    // const t1 = (new Date(item.auctionEndTimestamp)).getTime()


    return (<View style={{ marginRight: scale(10), padding: 10, marginBottom: 30, backgroundColor: colors.card, borderRadius: scale(10), borderRadius: 5, borderWidth: 0.5, borderColor: '#EBE7E7' }}>
        <TouchableOpacity onPress={() => props.navigation.navigate('MyNFTView', { item })}>
            <View style={{}}>
                <Image
                    source={item.image}
                    resizeMode="stretch"

                    style={{ height: 150, width: DeviceWidth / 1.2, borderRadius: 10 }}
                />

                <View style={{
                    position: 'absolute',
                    right: 5,
                    top: 5,
                }}>
                    <View style={{ backgroundColor: colors.card, padding: 10, borderRadius: 5 }}>
                        <Text style={{ fontSize: scale(14), color: 'red' }}>Live</Text>
                    </View>
                </View>
            </View>

            <View style={{ flexDirection: 'row', backgroundColor: colors.card, paddingLeft: 15, paddingRight: 15, padding: 10 }}>
                <View style={{ flex: 0.7, }}>
                    <Text style={{ fontSize: 14, fontWeight: 'bold', color: colors.text }}>{'MAD BOYS #3872349'}</Text>
                    <Text style={{ fontSize: 12, color: colors.text }}>{'Current bid with Vijai Sritharan '}</Text>

                </View>
                <View style={{ flex: 0.3, color: colors.text }}>
                    <Text style={{ fontSize: 12, color: colors.text }}>{'Highest Price'}</Text>
                    <Text style={{ fontSize: scale(16), color: colors.text }}>{'₹225.0'}</Text>

                </View>
            </View>


        </TouchableOpacity>
    </View>
    );
};

export const NFTListCollection = (props) => {
    const { NavigationTheme } = useTheme();
    const { colors } = NavigationTheme;
    const { itemDetails = {}, item = {} } = props;
    // const { itemDetails = {} } = item;
    const now = Date.now();
    // const t1 = (new Date(item.auctionEndTimestamp)).getTime()


    return (<View style={{ marginRight: scale(10), padding: 10, marginBottom: 30, backgroundColor: colors.card, borderRadius: scale(10), borderRadius: 5, borderWidth: 0.5, borderColor: '#EBE7E7' }}>
        <TouchableOpacity onPress={() => props.navigation.navigate('MyNFTView', { item })}>
            <View style={{}}>
                <Image
                    source={item.image}
                    resizeMode="stretch"

                    style={{ height: 150, width: DeviceWidth / 1.2, borderRadius: 10 }}
                />
            </View>

            <View style={{ backgroundColor: colors.card, paddingLeft: 15, paddingRight: 15, padding: 10 }}>
                <Text style={{ fontSize: 14, fontWeight: 'bold', color: colors.text }}>{'MAD BOYS #3872349'}</Text>
                <Text style={{ fontSize: 12, color: colors.text }}>{'Current bid with Vijai Sritharan '}</Text>


            </View>


        </TouchableOpacity>
    </View>
    );
};

export const NFTBidPriceSell = (props) => {
    const { NavigationTheme } = useTheme();
    const { colors } = NavigationTheme;


    return (<View style={{ backgroundColor: colors.card, padding: 15, margin: scale(10), borderRadius: scale(10) }}>
        <View style={{ flexDirection: 'row' }}>

            <View style={{ flex: 0.6 }}>
                <Text style={{ fontSize: scale(14), color: colors.text }}>{'Brought Price'}</Text>
                <Text style={{ fontSize: scale(22), color: colors.text, marginTop: scale(10) }}>{'₹ 125.0'}</Text>

            </View>

            <View style={{ flex: 0.4, alignItems: 'center', justifyContent: 'center' }}>
                <TouchableOpacity style={{ backgroundColor: '#0DB7B7', paddingLeft: 20, paddingRight: 20, padding: 10, borderRadius: 10 }} onPress={() => alert('Not Implemented yet')}>
                    <Text style={{ fontSize: scale(16), color: colors.text, fontWeight: '700' }}>{'Sell'}</Text>

                </TouchableOpacity>

            </View>

        </View>


    </View>
    );
};

export const FavourateNFT = (props) => {
    const { NavigationTheme } = useTheme();
    const { colors } = NavigationTheme;
    const { itemDetails = {}, item = {} } = props;
    // const { itemDetails = {} } = item;
    const now = Date.now();
    // const t1 = (new Date(item.auctionEndTimestamp)).getTime()
   


    return (<View style={{ marginRight: scale(10), padding: 10, marginBottom: 30, backgroundColor: colors.card, borderRadius: scale(10), borderRadius: 5, borderWidth: 0.5, borderColor: '#EBE7E7' }}>
        <TouchableOpacity style={{ padding: 10 }} onPress={() => props.navigation.navigate('Home', { screen: 'ViewLiveNFT',params: { item} })}>
            <View style={{ flex: 1, flexDirection: 'row', paddingBottom: scale(10) }}>
                <View style={{ flex: 1 }}>
                    <Image source={item.image}
                        resizeMode="stretch"
                        style={{ width: scale(20), height: scale(20), borderRadius: 15 }}
                    />

                </View>
                <View style={{ flex: 9 }}>

                    <Text style={{ color: colors.text, fontSize: scale(16), fontWeight: '400', }}>{'Aditi Musunur'}</Text>
                </View>
            </View>
            <View style={{}}>
                <Image
                    source={item.image}
                    resizeMode="stretch"

                    style={{ height: 150, width: DeviceWidth / 1.2, borderRadius: 10 }}
                />


            </View>

            <View style={{ flexDirection: 'row', backgroundColor: colors.card, paddingLeft: 15, paddingRight: 15, padding: 10 }}>
                <View style={{ flex: 0.7, }}>
                    <Text style={{ fontSize: 14, fontWeight: 'bold', color: colors.text }}>{'MAD BOYS #3872349'}</Text>
                    <Text style={{ fontSize: 12, color: colors.text }}>{'Current bid with Vijai Sritharan '}</Text>

                </View>
                <View style={{ flex: 0.3, color: colors.text }}>
                    <Text style={{ fontSize: 12, color: colors.text }}>{'Price'}</Text>
                    <Text style={{ fontSize: scale(16), color: colors.text }}>{'₹225.0'}</Text>

                </View>
            </View>


        </TouchableOpacity>
    </View>
    );
};