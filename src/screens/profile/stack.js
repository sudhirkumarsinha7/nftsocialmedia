import {createNativeStackNavigator} from '@react-navigation/native-stack';
import * as React from 'react';

import MyAccount from './MyAccount';
import MyCollections from './MyCollections';
import ExchangeToken from './ExchangeToken';
import Favourites from './Favourites';
import MyNFTView from './MyNFTView';
import FavFilter from './FavFilter';


import Profile from './index';
const Stack = createNativeStackNavigator();
const ProfileStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Profile"
        component={Profile}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="MyAccount"
        component={MyAccount}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="MyCollections"
        component={MyCollections}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="ExchangeToken"
        component={ExchangeToken}
        options={{
          headerShown: false,
        }}
      />
       <Stack.Screen
        name="MyNFTView"
        component={MyNFTView}
        options={{
          headerShown: false,
        }}
      />
       <Stack.Screen
        name="Favourites"
        component={Favourites}
        options={{
          headerShown: false,
        }}
      />
       <Stack.Screen
        name="FavFilter"
        component={FavFilter}
        options={{
          headerShown: false,
        }}
      />
    </Stack.Navigator>
  );
};
export default ProfileStack;
