/* eslint-disable react-native/no-color-literals */
/* eslint-disable sort-keys */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react';
import { useColorScheme, StyleSheet, Platform, Text, View, Image, TextInput, Switch, ScrollView,TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { scale } from '../../components/Scale';
import { localImage, imageslist, DeviceWidth } from '../../config/global';
import Feather from 'react-native-vector-icons/Feather';
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'

import { useIsFocused } from '@react-navigation/native';
import { Header } from '../../components/Header'
import LinearGradient from 'react-native-linear-gradient';
import { useDispatch } from 'react-redux'
import { changeTheme } from '../../redux/reducer/slice.reducer';
import { useTheme } from '../../theme/Hooks'
import {CustomTextView} from '../../components/Common/heper'

const getIcon =(name,color)=>{
  if(name==='My account'){
    return     <Feather name={'user'} color={color} size={25}/>
  }else if(name==='My Collections & Items'){
    return     <FontAwesome name={'image'} color={color} size={25}/>
  }else if(name==='Favourites'){
    return     <FontAwesome5 name={'file-invoice'} color={color} size={25}/>
  }else if(name==='Privacy'){
    return     <FontAwesome5 name={'lock'} color={color} size={25}/>
  }else if(name==='Security'){
    return     <FontAwesome5 name={'check-circle'} color={color} size={25}/>
  }else{
    return     <MaterialCommunityIcons name={'line-scan'} color={color} size={25}/>

  }

}
const CustomProfileHelper = (props) => {
    return (<View style={{padding:scale(10)}}><TouchableOpacity style={{flexDirection:'row',alignItems:'center'}} onPress={()=>alert('Not implemented yet')}>
      {getIcon(props.name,props.textColor)}
    <Text  style={{ color: props.textColor, fontSize: scale(18), fontWeight: '400', marginLeft: scale(10) }}>{props.name}</Text>

  </TouchableOpacity></View>)
}

const MyAccount = props => {
  const dispatch = useDispatch()
  const {NavigationTheme } = useTheme();
  const {colors}=NavigationTheme;
  const onChangeTheme = ({ theme, darkMode }) => {
    dispatch(changeTheme({ theme, darkMode }))
  }
  useEffect(() => {

  }, []);
const changeTheme1=(val)=>{
  // 
  onChangeTheme({ darkMode: val })
}

  return (
    <ScrollView >
      <Header
        navigation={props.navigation}
        name={'Home'}
      />
      <View style={{margin:10}}>
      <View style={{ alignItems: 'center' }}>
          <Image
            source={require('../../assets/Img/globe.png')}
            style={{ height: 150, width: 150 }}
          />
      
      </View>

      <View style={{ padding: scale(10), borderRadius: scale(15),}}>
      <Text style={{ color: colors.text, fontSize: scale(20), fontWeight: '900', }}>{'About you'}</Text>
        <CustomTextView leftText={'Name'} rightText={'Andrew Ainsley'} textColor={colors.text}/>
        <CustomTextView leftText={'Email'} rightText={'andrew@gmail.com'} textColor={colors.text}/>
        <CustomTextView leftText={'Joined'} rightText={'26 June, 2022'} textColor={colors.text}/>

      </View>

      <View style={{marginTop:scale(20)}}>
      <CustomProfileHelper 
            name={'Privacy'}
            textColor={colors.text}
            />
             <CustomProfileHelper 
            name={'Security'}
            textColor={colors.text}
            />
             <CustomProfileHelper 
            name={'Notifications'}
            textColor={colors.text}
            />
            
            
      </View>
      <View style={{flexDirection:'row',marginLeft:scale(10),marginTop:scale(10)}}>
      <MaterialCommunityIcons name={'eye'} color={colors.text} size={25}/>
      <View style={{flexDirection:'row',justifyContent:'space-between',flex:1}}>

      <Text style={{ color: colors.text, fontSize: scale(18), fontWeight: '400', marginLeft: scale(10) }}>Dark Mode</Text>
      <Switch
        trackColor={{ false: "#767577", true: "#FF4D67" }}
        thumbColor={NavigationTheme.dark ? "#f5dd4b" : "#f4f3f4"}
        ios_backgroundColor="#3e3e3e"
        onValueChange={(value)=>changeTheme1(value)}
        value={NavigationTheme.dark}
      />
        </View>
      </View>
      
      
            

      </View>


    </ScrollView>
  );
};

const styles = StyleSheet.create({
  pageContainer: {
    marginTop: Platform.OS === 'ios' ? scale(40) : 0
  },
});

function mapStateToProps(state) {
  return {


  };
}

export default connect(mapStateToProps, {

})(MyAccount);
