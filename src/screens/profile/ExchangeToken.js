/* eslint-disable react-native/no-color-literals */
/* eslint-disable sort-keys */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react';
import { ImageBackground, StyleSheet, Platform, Text, View, Image, TextInput, Switch, ScrollView,TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { scale } from '../../components/Scale';
import { localImage, imageslist, DeviceWidth } from '../../config/global';
import Foundation from 'react-native-vector-icons/Foundation'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import AntDesign from 'react-native-vector-icons/AntDesign'

import { useIsFocused } from '@react-navigation/native';
import { Header } from '../../components/Header'
import LinearGradient from 'react-native-linear-gradient';
import { useDispatch } from 'react-redux'
import { changeTheme } from '../../redux/reducer/slice.reducer';
import { useTheme } from '../../theme/Hooks'
import { CustomLinearBotton } from '../../components/Common/Button';


const ExchangeToken = props => {
  const dispatch = useDispatch()
  const {NavigationTheme } = useTheme();
  const {colors}=NavigationTheme;
  const [coin, setCoin] = useState('')

 
  useEffect(() => {

  }, []);
const coinExchangeView =()=>{
  return    <View style={{ flexDirection: 'row', borderColor: colors.text, borderWidth: 1, margin: scale(10), justifyContent: 'space-between', alignItems: 'center', borderRadius: scale(10), backgroundColor: colors.card,padding:scale(10)}}>
  <View style={{flexDirection:'row'}}>
  <Foundation name='bitcoin-circle' color={'yellow'} size={45}/>
  <View style={{marginLeft:scale(15)}}> 
  <Text style={{ fontSize: scale(12), fontWeight: '300', textAlign: 'center',color:colors.text }}>{'From'}</Text>
  <Text style={{ fontSize: scale(16), fontWeight: '700', textAlign: 'center',color:colors.text }}>{'STG'}</Text>

  </View>

  </View>
  <FontAwesome name='exchange' color={colors.text} size={45}/>

  <View style={{flexDirection:'row'}}>
 
  <View style={{marginRight:scale(15)}}> 
  <Text style={{ fontSize: scale(12), fontWeight: '300', textAlign: 'center',color:colors.text }}>{'TO'}</Text>
  <Text style={{ fontSize: scale(16), fontWeight: '700', textAlign: 'center',color:colors.text }}>{'BIT'}</Text>

  </View>
  <Foundation name='bitcoin-circle' color={'green'} size={45}/>
  </View>

  </View>
}

const cointConvertView =()=>{
  return<View>
                        <Text style={{ fontSize: scale(16), color: 'green', fontWeight: '700',margin: scale(10), }}>{'STATWIG TOKEN '}</Text>

                        <View style={{ flexDirection: 'row', borderColor: colors.text, borderWidth: 1, margin: scale(10), alignItems: 'center', borderRadius: scale(10), backgroundColor: colors.card, flex: 8 }}
      >
        

        <TextInput
          style={{ padding: scale(7), color: colors.text, fontSize: scale(20), flex: 1 }}
          value={coin}
          placeholderTextColor={colors.text}
          onChangeText={(text) => setCoin(text)}
        />
      </View>
</View>
}

const ConvertImgeView =()=>{
  return<View>
                        <Text style={{ fontSize: scale(16), color: colors.text, fontWeight: '700',margin: scale(10), }}>{'You will get '}</Text>
                        <View style={{alignItems:'center',borderRadius:10}}>
                      

                       
                        <ImageBackground
       source={localImage.Frame}
       style={{ height: 200, width: DeviceWidth / 1.1, borderRadius: 10 }}>
        <View style={{padding:20}}>
        <Text style={{ fontSize: scale(12), color: colors.text, fontWeight: '400',}}>{'Discover ways to earn crypto'}</Text>

        <Text style={{ fontSize: scale(16), color: colors.text, fontWeight: '700', }}>{'BIT COIN'}</Text>
        <AntDesign name='arrowright' color={colors.text} size={45}/>

        <Text style={{ fontSize: scale(16), color: colors.text, fontWeight: '700',marginTop:scale(40) }}>{coin+ 'Coins'}</Text>

        </View>
      </ImageBackground>
      </View>
                       
                        
</View>
}

  return (
    <ScrollView >
      <Header
        navigation={props.navigation}
        name={'Home'}
      />
     <View style={{margin:10}}>
  {coinExchangeView()}
  {cointConvertView()}
  {coin?ConvertImgeView():null}
  <CustomLinearBotton
    title={'StartConvert'}
    onNavigate={() =>alert('Not implemented')}
  />
     </View>


    </ScrollView>
  );
};

const styles = StyleSheet.create({
  pageContainer: {
    marginTop: Platform.OS === 'ios' ? scale(40) : 0
  },
});

function mapStateToProps(state) {
  return {


  };
}

export default connect(mapStateToProps, {

})(ExchangeToken);
