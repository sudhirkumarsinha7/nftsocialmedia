/* eslint-disable react-native/no-color-literals */
/* eslint-disable sort-keys */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react';
import { useColorScheme, StyleSheet, Platform, Text, View, Image, TextInput, Switch, ScrollView,TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { scale } from '../../components/Scale';
import { localImage, imageslist, DeviceWidth } from '../../config/global';
import Feather from 'react-native-vector-icons/Feather';
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import { useIsFocused } from '@react-navigation/native';
import { Colors } from 'react-native-paper';
import { CustomLinearBotton, CustomLinearBotton1 } from '../../components/Common/Button';
import { Header } from '../../components/Header'
import LinearGradient from 'react-native-linear-gradient';
import { useDispatch } from 'react-redux'
import { changeTheme } from '../../redux/reducer/slice.reducer';
import { useTheme } from '../../theme/Hooks'

const getIcon =(name,color)=>{
  if(name==='My account'){
    return     <Feather name={'user'} color={color} size={25}/>
  }else if(name==='My Collections & Items'){
    return     <FontAwesome name={'image'} color={color} size={25}/>
  }else if(name==='Favourites'){
    return     <FontAwesome5 name={'file-invoice'} color={color} size={25}/>
  }else if(name==='Exchange Tokens'){
    return     <FontAwesome5 name={'file-invoice'} color={color} size={25}/>
  }else if(name==='Invite Friends'){
    return     <FontAwesome5 name={'file-invoice'} color={color} size={25}/>
  }else{
    return     <FontAwesome5 name={'file-invoice'} color={color} size={25}/>

  }

}
const CustomProfileHelper = (props) => {
    return (<View style={{padding:scale(10)}}><TouchableOpacity style={{flexDirection:'row',alignItems:'center'}} onPress={()=>props.onNavigate()}>
      {getIcon(props.name,props.textColor)}
    <Text  style={{ color: props.textColor, fontSize: scale(18), fontWeight: '400', marginLeft: scale(10) }}>{props.name}</Text>

  </TouchableOpacity></View>)
}

const ProfileScreen = props => {
  const dispatch = useDispatch()
  const {NavigationTheme } = useTheme();
  const {colors}=NavigationTheme;
  const onChangeTheme = ({ theme, darkMode }) => {
    dispatch(changeTheme({ theme, darkMode }))
  }
  useEffect(() => {

  }, []);
const changeTheme1=(val)=>{
  // 
  onChangeTheme({ darkMode: val })
}

  return (
    <ScrollView >
      <Header
        navigation={props.navigation}
        name={'Home'}
      />
      <View style={{margin:10}}>
      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
        <View style={{ flex: 0.4 }}>
          <Image
            source={require('../../assets/Img/globe.png')}
            style={{ height: 150, width: 150 }}
          />
        </View>
        <View style={{ flex: 0.6 }}>
          <Text style={{ color: colors.text, fontSize: scale(18), fontWeight: '700', }}>{'Gift Habeshaw'}</Text>
          <Text style={{ color: colors.text, fontSize: scale(16), fontWeight: '400', marginTop: scale(10) }}>{'52fs5ge5g45sov45a'}</Text>
        </View>
      </View>

      <View style={{ backgroundColor:colors.card, padding: scale(10), borderRadius: scale(15), marginTop: 15 }}>
        <View style={{ flexDirection: 'row' }}>
          <View style={{ flex: 0.9 }}>


            <Text style={{ color: colors.text, fontSize: scale(16), fontWeight: '400', marginLeft: scale(10) }}>{'Balance'}</Text>

            <Text style={{ color: colors.text, fontSize: scale(24), fontWeight: '400', marginLeft: scale(10) }}>{'5.000 STG'}</Text>
          </View>
          <LinearGradient
            start={{ x: 0, y: 0 }}
            end={{ x: 1, y: 0 }}
            colors={['#0038F5', '#9F03FF']}

            style={{
              borderRadius: scale(10),
              padding: scale(13),
              margin: scale(10)

            }}>
            <TouchableOpacity onPress={() => alert('Not Implemented yet')}>
              <Text style={{ color: colors.text, fontSize: scale(16), fontWeight: '900', marginLeft: scale(10) }}>Add</Text>
            </TouchableOpacity>
          </LinearGradient>
        </View>
      </View>

      <View style={{marginTop:scale(20)}}>
      <CustomProfileHelper 
            name={'My account'}
            textColor={colors.text}
            onNavigate={() => props.navigation.navigate('MyAccount')}
            />
             <CustomProfileHelper 
            name={'My Collections & Items'}
            textColor={colors.text}
            onNavigate={() => props.navigation.navigate('MyCollections')}
            />
             <CustomProfileHelper 
            name={'Favourites'}
            textColor={colors.text}
            onNavigate={() => props.navigation.navigate('Favourites')}
            />
             <CustomProfileHelper 
            name={'Exchange Tokens'}
            textColor={colors.text}
            onNavigate={() => props.navigation.navigate('ExchangeToken')}

            />
             <CustomProfileHelper 
            name={'Invite Friends'}
            textColor={colors.text}
            onNavigate={() => props.navigation.navigate('ExchangeToken')}
            />
            
            <CustomProfileHelper 
            name={'Help & FAQ'}
            textColor={colors.text}
            onNavigate={() => props.navigation.navigate('Community', { screen: 'FaqSupport' })}
            />
      </View>
      <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:scale(20)}}>
      <Text style={{ color: colors.text, fontSize: scale(18), fontWeight: '400', marginLeft: scale(10) }}>Dark Mode</Text>
      <Switch
        trackColor={{ false: "#767577", true: "#FF4D67" }}
        thumbColor={NavigationTheme.dark ? "#f5dd4b" : "#f4f3f4"}
        ios_backgroundColor="#3e3e3e"
        onValueChange={(value)=>changeTheme1(value)}
        value={NavigationTheme.dark}
      />
      </View>
      
      
            

      </View>


    </ScrollView>
  );
};

const styles = StyleSheet.create({
  pageContainer: {
    marginTop: Platform.OS === 'ios' ? scale(40) : 0
  },
});

function mapStateToProps(state) {
  return {


  };
}

export default connect(mapStateToProps, {

})(ProfileScreen);
