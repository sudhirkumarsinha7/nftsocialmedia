import * as React from 'react';
import {View, Text,ScrollView} from 'react-native';
import { HeaderWithBack } from '../../components/Header'
import {NFTImgeView,NFTSoldPrice,NFTActivity,NFTBidPrice} from '../home/helper'
import {NFTBidPriceSell} from './helper'

const ViewLiveNFT = (props) => {
    const nftDetails = props.route.params.item;

  return (
    <ScrollView >
        <HeaderWithBack
        navigation={props.navigation}
        name={'NFT Details'}
      />
      <NFTImgeView 
      item={nftDetails}
      />
           <NFTBidPriceSell  item={nftDetails}/>

      
    </ScrollView>

  );
};
export default ViewLiveNFT;