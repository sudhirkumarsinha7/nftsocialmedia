/* eslint-disable react-native/no-color-literals */
/* eslint-disable sort-keys */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react';
import { useColorScheme, StyleSheet, Platform, Text, View, Image, TextInput, FlatList, ScrollView,TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { localImage, imageslist, DeviceWidth } from '../../config/global';
import Feather from 'react-native-vector-icons/Feather';
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { moderateScale, scale, verticalScale } from '../../components/Scale';

import { useIsFocused } from '@react-navigation/native';
import { Header } from '../../components/Header'
import LinearGradient from 'react-native-linear-gradient';
import { useDispatch } from 'react-redux'
import { changeTheme } from '../../redux/reducer/slice.reducer';
import { useTheme } from '../../theme/Hooks'
import {NFTListCollection,LiveNFTList} from './helper'
import {SelectionSelectTab} from '../../components/SelectionSelect'
let radioItems = [
  {
    label: 'My Collections',
    selected: true,
  },
  {
    label: 'For Sales',
    selected: false,
  },
]


const MyAccount = props => {
  const dispatch = useDispatch()
  const {NavigationTheme } = useTheme();
  const {colors}=NavigationTheme;
  const [selectedTab, setTab] = useState('My Collections');
  const onChangeTheme = ({ theme, darkMode }) => {
    dispatch(changeTheme({ theme, darkMode }))
  }
  const changeActiveRadioButton = (index) => {
    radioItems.map(item => {
      item.selected = false;
    });

    radioItems[index].selected = true;
    setTab(radioItems[index].label)
  }
  useEffect(() => {

  }, []);
const myList =()=>{
  return <View>
 <FlatList
          onEndReachedThreshold={0.7}
          keyExtractor={(item, index) => index.toString()}
          data={imageslist}
          renderItem={({ item }) => eachList(item)}
        />
  </View>
}
const eachList =(item)=>{
  return<View>
      <NFTListCollection item={item}  navigation={props.navigation}/>
  </View>
}
const liveList =()=>{
  return <View>
     <FlatList
          onEndReachedThreshold={0.7}
          keyExtractor={(item, index) => index.toString()}
          data={imageslist}
          renderItem={({ item }) => eachliveList(item)}
        />
  </View>
}
const eachliveList =(item)=>{
  return<View>
      <LiveNFTList item={item}  navigation={props.navigation}/>
  </View>
}
  return (
    <ScrollView >
      <Header
        navigation={props.navigation}
        name={'Home'}
      />
      <View style={{margin:10}}>
      <View style={{ alignItems: 'center' }}>
          <Image
            source={require('../../assets/Img/globe.png')}
            style={{ height: 150, width: 150 }}
          />
      
      </View>

      <View style={{ padding: scale(10),}}>
      <Text style={{ color: colors.text, fontSize: scale(20), fontWeight: '900',alignSelf:'center' }}>{'Gift Habeshaw'}</Text>
       
      <Text style={{ color: colors.text, fontSize: scale(14), fontWeight: '400',alignSelf:'center',marginTop:scale(15) }}>{'Meka from the MekaVerse - A collection of 8,888 unique '}</Text>

      </View>

      <View style={{marginTop:scale(5)}}>
      <View style={{ flexDirection: 'row', marginBottom: verticalScale(10), elevation: 5 }}>
        {radioItems.map((item, key) => (
          <SelectionSelectTab
            key={key}
            button={item}
            onClick={() => changeActiveRadioButton(key)}
            selectedBackgraoundColor={'#84FFE9'}
            unSelectedTextColor={'green'}
          />
        ))}
      </View>
            
      </View>
      
      {selectedTab === 'My Collections' ? myList() : liveList()}

      
            

      </View>


    </ScrollView>
  );
};

const styles = StyleSheet.create({
  pageContainer: {
    marginTop: Platform.OS === 'ios' ? scale(40) : 0
  },
});

function mapStateToProps(state) {
  return {


  };
}

export default connect(mapStateToProps, {

})(MyAccount);
