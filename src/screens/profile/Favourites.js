import React, { useEffect, useState } from 'react';
import { useColorScheme, StyleSheet, Platform, Text, View, Image, TextInput, FlatList, ScrollView, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { GallaryList, imageslist, DeviceWidth } from '../../config/global';
import Feather from 'react-native-vector-icons/Feather';
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import { moderateScale, scale, verticalScale } from '../../components/Scale';

import { useIsFocused } from '@react-navigation/native';
import { Header } from '../../components/Header'
import LinearGradient from 'react-native-linear-gradient';
import { useDispatch } from 'react-redux'
import { changeTheme } from '../../redux/reducer/slice.reducer';
import { useTheme } from '../../theme/Hooks'
import { NFTListCollection, FavourateNFT } from './helper'
import { SelectionSelectTab } from '../../components/SelectionSelect'

const Favourites = props => {
  const dispatch = useDispatch()
  const { NavigationTheme } = useTheme();
  const { colors } = NavigationTheme;
  const [searchText, setSearchText] = useState('')

  useEffect(() => {

  }, []);
  const eachFav = (item) => {
    return <View>
      <FavourateNFT item={item} navigation={props.navigation} />
    </View>
  }

  return (
    <ScrollView >
      <Header
        navigation={props.navigation}
        name={'Home'}
      />

      <View style={{ margin: 10 }}>
        <View style={{ backgroundColor: '#84FFE9', paddingLeft: 20, paddingRight: 20, padding: 10, borderRadius: 10 }}>
          <Text style={{ fontSize: scale(16), fontWeight: '700', textAlign: 'center' }}>{'FAVOURITIES'}</Text>

        </View>

        <View style={{ flexDirection: 'row' }}>
          <View style={{ flexDirection: 'row', borderColor: colors.text, borderWidth: 1, margin: scale(10), alignItems: 'center', borderRadius: scale(10), backgroundColor: colors.card, flex: 8 }}
          >
            <TouchableOpacity
              onPress={() => alert('Not Implemented yet')}
              style={{ marginLeft: 10 }}
            >
              <Feather name="search" size={25} color={colors.text} />
            </TouchableOpacity>

            <TextInput
              style={{ padding: scale(7), color: colors.text, fontSize: scale(20), flex: 1 }}
              value={searchText}
              placeholder={'Search items, collections, and accounts'}
              placeholderTextColor={colors.text}
              onChangeText={(text) => setSearchText(text)}
            />
            <TouchableOpacity
              style={{ marginRight: 10 }}
              onPress={() => alert('Not Implemented yet')}
            >
              <MaterialIcons name="keyboard-voice" size={25} color={colors.text} />
            </TouchableOpacity>
          </View>

          <TouchableOpacity style={{ alignItems: 'center', justifyContent: 'center' }} onPress={() => props.navigation.navigate('FavFilter')}>
            <FontAwesome name="filter" size={25} color={colors.text} />

          </TouchableOpacity>
        </View>

        <FlatList
          onEndReachedThreshold={0.7}
          keyExtractor={(item, index) => index.toString()}
          data={GallaryList}
          renderItem={({ item }) => eachFav(item)}
        />
      </View>

    </ScrollView>
  );
};

const styles = StyleSheet.create({
  pageContainer: {
    marginTop: Platform.OS === 'ios' ? scale(40) : 0
  },
});

function mapStateToProps(state) {
  return {


  };
}

export default connect(mapStateToProps, {

})(Favourites);
