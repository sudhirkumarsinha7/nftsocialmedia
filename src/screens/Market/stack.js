import {createNativeStackNavigator} from '@react-navigation/native-stack';
import * as React from 'react';


import Market from './index';
import ViewMarketPlace from './viewMarketPlace';
import MarketFilter from './MarketFilter'
const Stack = createNativeStackNavigator();
const MarketScreen = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Market"
        component={Market}
        options={{
          headerShown: false,
        }}
      />
       <Stack.Screen
        name="ViewMarketPlace"
        component={ViewMarketPlace}
        options={{
          headerShown: false,
        }}
      />
       <Stack.Screen
        name="MarketFilter"
        component={MarketFilter}
        options={{
          headerShown: false,
        }}
      />
    </Stack.Navigator>
  );
};
export default MarketScreen;
