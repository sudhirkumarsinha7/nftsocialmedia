/* eslint-disable react-native/no-color-literals */
/* eslint-disable sort-keys */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react';
import { useColorScheme, StyleSheet, Platform, Text, View, Image, TextInput, Switch, ScrollView,TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { scale } from '../../components/Scale';
import { localImage, imageslist, DeviceWidth } from '../../config/global';
import Feather from 'react-native-vector-icons/Feather';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import AntDesign from 'react-native-vector-icons/AntDesign'
import { useIsFocused } from '@react-navigation/native';
import { Header } from '../../components/Header'
import LinearGradient from 'react-native-linear-gradient';
import { useDispatch } from 'react-redux'
import { changeTheme } from '../../redux/reducer/slice.reducer';
import { useTheme } from '../../theme/Hooks'
import { CustomLinearBotton2, CustomLinearBotton1 } from '../../components/Common/Button';
let TypeList = [
  {
    label: 'Fixed Item',
    selected: true,
  },
  {
    label: 'Auction Item',
    selected: false,
  },
]

const FilterHeader = (props) => {
  const {NavigationTheme } = useTheme();
  const {colors}=NavigationTheme;

  
    return ( <View style={{flexDirection:'row'}}>
    <View style={{flex:9}}>
    <Text style={{ fontSize: scale(16), color: colors.text, fontWeight: '700' }}>{'FILTERS'}</Text>

    </View>
    <TouchableOpacity style={{flex:1,}} onPress={()=>props.navigation.goBack()}>
  <AntDesign name='closecircle' size={30} color={colors.text}/>
</TouchableOpacity>
  </View>)
}

const ExchangeToken = props => {
  const dispatch = useDispatch()
  const {NavigationTheme } = useTheme();
  const {colors}=NavigationTheme;
  const [searchText, setSearchText] = useState('')

  const [fixedFiter, setFixtedFiter] = useState(false);
  const [auctionFilter, setAutionFiter] = useState(false);
  const [ethFiter, setEthFiter] = useState(false);
  const [wethFilter, setWethFiter] = useState(false);
  const [numFilter, setNumFilterr] = useState(false);

  useEffect(() => {

  }, []);

  const TypeFilterView =()=>{
    return <View>
                          <Text style={{ fontSize: scale(16), color: colors.text, fontWeight: '700' }}>{'TYPE'}</Text>

    <View style={{flexDirection: 'row'}}>
    <CustomLinearBotton2
    title={'Fixed Item'}
    onNavigate={() => setFixtedFiter(!fixedFiter)}
    isColorful={fixedFiter}
  />
  <CustomLinearBotton2
    title={'Auction Item'}
    onNavigate={() => setAutionFiter(!auctionFilter)}
    isColorful={auctionFilter}
  />
    </View>
 </View>
  }
    const OnSaleFilterView =()=>{
      return<View>
                            <Text style={{ fontSize: scale(16), color: colors.text, fontWeight: '700' }}>{'Onsale in '}</Text>

      <View style={{flexDirection: 'row'}}>
      <CustomLinearBotton2
      title={'ETH'}
      onNavigate={() => setEthFiter(!ethFiter)}
      isColorful={ethFiter}
    />
    <CustomLinearBotton2
      title={'WETH'}
      onNavigate={() => setWethFiter(!wethFilter)}
      isColorful={wethFilter}
    />
    <CustomLinearBotton2
      title={'0xBTC'}
      onNavigate={() => setNumFilterr(!numFilter)}
      isColorful={numFilter}
    />
      </View>
   </View>
    }
    const serachArtish =()=>{
      return<View>
                            <Text style={{ fontSize: scale(16), color: colors.text, fontWeight: '700' }}>{'Artist Name '}</Text>

                            <View style={{ flexDirection: 'row', borderColor: colors.text, borderWidth: 1, margin: scale(10), alignItems: 'center', borderRadius: scale(10), backgroundColor: colors.card, flex: 8 }}
          >
            <TouchableOpacity
              onPress={() => alert('Not Implemented yet')}
              style={{ marginLeft: 10 }}
            >
              <Feather name="search" size={25} color={colors.text} />
            </TouchableOpacity>

            <TextInput
              style={{ padding: scale(7), color: colors.text, fontSize: scale(20), flex: 1 }}
              value={searchText}
              placeholder={'Search items, collections, and accounts'}
              placeholderTextColor={colors.text}
              onChangeText={(text) => setSearchText(text)}
            />
            <TouchableOpacity
              style={{ marginRight: 10 }}
              onPress={() => alert('Not Implemented yet')}
            >
              <MaterialIcons name="keyboard-voice" size={25} color={colors.text} />
            </TouchableOpacity>
          </View>
   </View>
    }
  return (
    <ScrollView >
      <Header
        navigation={props.navigation}
        name={'Home'}
      />
     <View style={{margin:10}}>

     <FilterHeader 
     navigation={props.navigation}
     />
    {TypeFilterView()}
    {serachArtish()}

 {OnSaleFilterView()}



     
     </View>


    </ScrollView>
  );
};

const styles = StyleSheet.create({
  pageContainer: {
    marginTop: Platform.OS === 'ios' ? scale(40) : 0
  },
});

function mapStateToProps(state) {
  return {


  };
}

export default connect(mapStateToProps, {

})(ExchangeToken);
