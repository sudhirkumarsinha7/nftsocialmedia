/* eslint-disable react-native/no-color-literals */
/* eslint-disable sort-keys */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react';
import { useColorScheme, StyleSheet, Platform, Text, View, Image, TextInput, FlatList, ScrollView,TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { scale } from '../../components/Scale';
import { GallaryList, ModalList, DeviceWidth } from '../../config/global';
import Feather from 'react-native-vector-icons/Feather';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import AntDesign from 'react-native-vector-icons/AntDesign'
import { useIsFocused } from '@react-navigation/native';
import { Colors } from 'react-native-paper';
import { CustomLinearBotton, CustomLinearBotton1 } from '../../components/Common/Button';
import { Header } from '../../components/Header'
import { useSafeAreaFrame } from 'react-native-safe-area-context';
import { useTheme } from '../../theme/Hooks'

const MarketScreen = props => {
  const isFocused = useIsFocused();
  const { NavigationTheme } = useTheme();
  const { colors } = NavigationTheme;
  useEffect(() => {

  }, []);
  const eachArtist =(item)=>{
    return <TouchableOpacity style={{padding:5,borderRadius:10,marginRight:scale(10)}} onPress={() => props.navigation.navigate('Home', { screen: 'ViewArtist',params: { ArtistDetails: item} })}>
        <View>
        <Image source={item.image} 
        resizeMode={'stretch'}
        style={{ width: scale(100), height: scale(100),borderRadius:10 }} 
        />

        </View>
        <View  style={{backgroundColor:colors.card,padding:7,marginTop:scale(-30),margin:10,borderRadius:10,justifyContent:'center'}}>
        <Text style={{ color: colors.text, fontSize: scale(10), fontWeight: '400',textAlign:'center' }}>{'Aditi Sharma'}</Text>

        </View>
    </TouchableOpacity>
  } 
  const eachSaleList =(item)=>{
    return <TouchableOpacity style={{flexDirection:'row', padding:10,borderRadius:10,marginRight:scale(10),}} onPress={() => props.navigation.navigate('Home', { screen: 'ViewSoldNF',params: { item} })}>
        <View style={{flex:0.2}}>
        <Image source={item.image} 
        resizeMode={'stretch'}
        style={{ width: scale(50), height: scale(50),borderRadius:10 }} 
        />

        </View>
        <View style={{flex:0.8,}}>
        <Text style={{ color: colors.text, fontSize: scale(14), fontWeight: '700',}}>{'SEE YOU SOON #123786'}</Text>
        <View style={{flexDirection:'row',alignItems:'center'}}>
          <View style={{flex:8}}>

          
        <Text style={{ color: colors.text, fontSize: scale(12), fontWeight: '400',}}>{'Created By:'}   <Text style={{ color: '#1098FC', fontSize: scale(12), fontWeight: '400',}}>{'Jayadev Mitali '}</Text>   </Text>
        </View>
        <View>
        <Text style={{ color: colors.text, fontSize: scale(12), fontWeight: '400',}}>{'Price'}</Text>
        <Text style={{ color: colors.text, fontSize: scale(14), fontWeight: '700',}}>{'₹375.0'}</Text>

        </View>
      </View>
        </View>
    </TouchableOpacity>
  }
  const MarketListComponent=(item)=>{
    return <View>
     <View>
          <View style={{ marginTop: scale(5) }}>

            <Text style={{ color: colors.text, fontSize: scale(22), fontWeight: '900', textAlign: 'center' }}>{'MARKET'}</Text>
            <Text style={{ color: colors.text, fontSize: scale(14), fontWeight: '400', textAlign: 'center', marginTop: scale(15) }}>{'Grow your NFT collection by buying and selling with other users.'}</Text>
            <Text style={{ color: '#32DAFF', fontSize: scale(18), fontWeight: '400', textAlign: 'center', marginTop: scale(15) }}>{'Get them on the marketplace!'}</Text>


          </View>
          <View style={{ marginTop: scale(5), alignItems: 'center', backgroundColor: colors.card, padding: 10, borderRadius: 10 }}>
            <Image
              source={require('../../assets/Img/cover.png')}
              style={{ height: 200, width: DeviceWidth / 1.1, borderRadius: 10 }}
              resizeMode={'stretch'}
            />
            <View style={{ marginTop: -80, alignItems: 'center', }}>

              <Image
                source={require('../../assets/Img/nft.png')}
                style={{ height: 150, width: 150, }}
                resizeMode={'stretch'}
              />
            </View>
            <Text style={{ color: colors.text, fontSize: scale(22), fontWeight: '900', textAlign: 'center' }}>{'MARKETPLACE'}</Text>
            <Text style={{ color: colors.text, fontSize: scale(14), fontWeight: '400', textAlign: 'center', marginTop: scale(15) }}>{'Buy and sell licensed nft on our marketplace'}</Text>

            <CustomLinearBotton
              title={'View'}
              onNavigate={() => props.navigation.navigate('ViewMarketPlace')}

            />
          </View>
        </View>

        <View>
          <View style={{ marginTop: scale(15), flexDirection: 'row', alignItems: 'center' }}>

            <Text style={{ color: colors.text, fontSize: scale(20), fontWeight: '700', flex: 1 }}>{'TOP COLLECTORS'}</Text>
            <TouchableOpacity
              onPress={() => alert('Not Implemented yet')}
            >
              <AntDesign name="arrowright" size={40} color={colors.text} />
            </TouchableOpacity>
          </View>
          <FlatList
            // onEndReachedThreshold={0.7}
            horizontal={true}
            keyExtractor={(item, index) => index.toString()}
            data={ModalList}
            renderItem={({ item }) => eachArtist(item)}

          />
        </View>
        <View>
          <View style={{ marginTop: scale(15), flexDirection: 'row', alignItems: 'center' }}>

            <Text style={{ color: colors.text, fontSize: scale(20), fontWeight: '700', flex: 1 }}>{'LATEST SALES'}</Text>
            <TouchableOpacity
              onPress={() => alert('Not Implemented yet')}
            >
              <AntDesign name="arrowright" size={40} color={colors.text} />
            </TouchableOpacity>
          </View>
          <FlatList
            // onEndReachedThreshold={0.7}
            horizontal={false}
            keyExtractor={(item, index) => index.toString()}
            data={GallaryList}
            renderItem={({ item }) => eachSaleList(item)}

          />
        </View>

        <View>
          <View style={{ alignItems: 'center' }}>

            <Image
              style={{ height: 60, width: DeviceWidth / 1.4 }}
              resizeMode="stretch"
              source={require('../../assets/Img/logo.png')}
            />
            <Text style={{ color: colors.text, fontSize: scale(22), fontWeight: '700', textAlign: 'center' }}>{'The New NFT MARKETPLACE'}</Text>

          </View>

          <CustomLinearBotton
            title={'Discover more'}
            onNavigate={() => props.navigation.navigate('Community', { screen: 'Discover' })}
          />
          <CustomLinearBotton1
            title={'FAQ’s & Support'}
            onNavigate={() => props.navigation.navigate('Community', { screen: 'FaqSupport' })}
          />
        </View>
    </View>
  }
  return (
    <View >
      <Header
        navigation={props.navigation}
        name={'Home'}
      />
      <View style={{ margin: 10 }}>

      <FlatList
            onEndReachedThreshold={0.7}
            style={{ width: '100%' }}
            keyExtractor={(item, index) => index.toString()}
            data={['test']}
            renderItem={({ item, index }) => MarketListComponent(item, index)}
            ListFooterComponent={<View style={{ height: DeviceWidth }} />}

        />
        
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  pageContainer: {
    marginTop: Platform.OS === 'ios' ? scale(40) : 0
  },
});

// export default MarketScreen;
function mapStateToProps(state) {
  return {


  };
}

export default connect(mapStateToProps, {

})(MarketScreen);
