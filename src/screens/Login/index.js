import React,{useState,useEffect} from "react";
import { ImageBackground, View, Text, StyleSheet, TouchableOpacity,Image,TextInput } from 'react-native'
import { Colors } from "react-native-paper";
import { localImage, imageslist } from '../../config/global';
import { DeviceWidth } from "../../config/global";
import { scale } from '../../components/Scale';
import WalletConnectProvider from '@walletconnect/web3-provider';
import { useWalletConnect, withWalletConnect } from '@walletconnect/react-native-dapp';
import { NavigationContainer, useIsFocused } from '@react-navigation/native';
import { ethers } from 'ethers';

const Login = (props) => {
    const [isLoginScreen,setIsLoginScreen] = useState(false)
    const isFocused = useIsFocused();
    const connector = useWalletConnect(); // valid
    const [connected, setconnected] = useState(false)
    const [walletAddress, SetWalletAddress] = useState('')
    useEffect(() => {
      if (connector  && connector.accounts && connector.accounts.length && connector.accounts[0]) {
        setconnected(true)
      }
    }, [isFocused]);
  
    const onConnect = async () => {
      connector.connect()
      const provider = new WalletConnectProvider({
        bridge: "https://bridge.walletconnect.org",
        rpc: { 1: "https://rpc.ankr.com/eth", 3: "https://ropsten.infura.io/v3/9aa3d95b3bc440fa88ea12eaa4456161" },
        infuraId: "8043bb2cf99347b1bfadfb233c5325c0",
        connector: connector,
        connected: true,
        qrcode: false,
      });
      await provider.enable();
      const ethers_provider = new ethers.providers.Web3Provider(provider);
      setWalletAddress(connector.accounts[0])
      setconnected(true)
    }
  
  
    const DescribeView =()=>{
        return <View>
<View style={{ marginTop: DeviceWidth * 1.3, margin: 20 }}>
            <Text style={{ fontWeight: '900', fontSize: 30, color: Colors.white }}>Discover Rare Video and Music and collect NFT's</Text>
            <Text style={{ fontSize: 20, color: Colors.white, marginTop: 30 }}>A crediable and excelent MarketPlace for nan fungable Token</Text>
            <TouchableOpacity onPress={() => setIsLoginScreen(true)} style={{ padding: 15, backgroundColor: Colors.red600, borderRadius: 30, marginTop: 50, marginLeft: 20, marginRight: 20 }}>
                <Text style={{ color: Colors.white, textAlign: 'center', fontWeight: '600', fontSize: 24 }}>Get started</Text>
            </TouchableOpacity>

        </View>
        </View>
    }

    const LoginView=()=>{
        return <View>
<View style={{ margin: 10, marginTop: Platform.OS === 'ios' ? scale(40) : 0 }}>
      <View style={{
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: scale(50)
      }}>
        <Image
          style={{
            width: 200,
            height: 200,
            borderRadius: 5,
            backgroundColor: 'green',
            marginBottom: scale(20)
          }}
          source={require('../../assets/wallet.png')}
        />
        <Text style={{ fontSize: scale(20), fontWeight: 'bold', color: Colors.red900 }}>Login into your Wallet</Text>
        <Text style={{ fontSize: scale(16), marginTop: scale(20) }}>Connect to any WalletConnect supported wallet to securely buy your digital goods and support your Favarotes actors</Text>

      </View>

      <TouchableOpacity onPress={() => onConnect()} style={{ backgroundColor: Colors.blue600, padding: 10, borderRadius: scale(30), marginTop: scale(30), margin: 10 }}>
        <Text style={{ fontSize: scale(18), color: Colors.white, textAlign: 'center', fontWeight: 'bold' }}>Connect Wallet</Text>

      </TouchableOpacity>
      
    </View>
        </View>
    }

    return (<ImageBackground
        style={styles.imgBackground}
        resizeMode="stretch"
        source={localImage.backgroundImage}>
        {/* {DescribeView()} */}
        {LoginView()}
    </ImageBackground>)

}
const styles = StyleSheet.create({
    imgBackground: {
        width: '100%',
        height: '100%',
    },

});

export default Login