/* eslint-disable sort-keys */
/* eslint-disable @typescript-eslint/no-unused-vars */
import {USERINFO,LOGIN,REGISTER} from '../constant';
import axios from 'axios';

import {config, getIosDeviceTokeApi,auth} from '../../config/config';

import setAuthToken from '../../config/setAuthToken';
import {loadingOff, loadingOn} from './loader';

export const Register = (data, callback = () => { }) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    console.log('Action_sRegister_data' + JSON.stringify(data));
    let response = await axios.post(config().register, data);

    console.log('Action_sRegister_response' + JSON.stringify(response));
    return response;
  } catch (e) {
    console.log('Action_Register_error' + JSON.stringify(e));

    return e.response;
  } finally {
    loadingOff(dispatch);
  }

};
export const LogIn = (data, callback = () => { }) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    console.log('Action_sLogIn_data' + JSON.stringify(data));
    let response = await axios.post(config().login, data);

    console.log('Action_LogIn_response' + JSON.stringify(response));
    dispatch({
      type: USERINFO,
      payload: response.data.data,
    });
    setAuthToken(response.data && response.data.data && response?.data?.data?.token)
    return response;
  } catch (e) {
    console.log('Action_LogIn_error' + JSON.stringify(e));

    return e.response;
  } finally {
    loadingOff(dispatch);
  }

};
export const ResetUserInfo = () => async (dispatch) => {
  dispatch({
    type: USERINFO,
    payload: {},
  }); 
};
export const DeleteAccount = () => async (dispatch) => {
  loadingOn(dispatch);
  try {
    let response = await axios.post(config().deleteAccount);

    console.log('Action_sDeleteAccount_response' + JSON.stringify(response));
    return response;
  } catch (e) {
    console.log('Action_DeleteAccount_error' + JSON.stringify(e));

    return e.response;
  } finally {
    loadingOff(dispatch);
  }
};