import {
    COLLECTION,
    ON_SALE,
    FAV,
    PURCHASED,
    UNLISTED,
    WALLET_ADDRESS
} from '../constant';

const initialState = {
    user_fav: [],
    user_collection: [],
    user_purchase: [],
    user_unlisted: [],
    user_created: [],
    walletAddress: '',
};
export default function (state = initialState, action) {
    switch (action.type) {
        case COLLECTION:
            return {
                ...state,
                user_collection: action.payload,
            };
        case ON_SALE:
            return {
                ...state,
                user_created: action.payload,
            };
        case FAV:
            return {
                ...state,
                user_fav: action.payload,
            };
        case PURCHASED:
            return {
                ...state,
                user_purchase: action.payload,
            };
        case UNLISTED:
            return {
                ...state,
                user_unlisted: action.payload,
            };
        case WALLET_ADDRESS:
            return {
                ...state,
                walletAddress: action.payload,
            };
        default:
            return state;
    }
}
