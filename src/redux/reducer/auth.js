import {USERINFO} from '../constant';
const initialState = {
  userInfo:{}
};
export default function (state = initialState, action) {
  switch (action.type) {
    case USERINFO:
      return {
        ...state,
        userInfo: action.payload,
      };
    default:
      return state;
  }
}
