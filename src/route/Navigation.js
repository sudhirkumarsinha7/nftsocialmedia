import React,{useEffect} from 'react';
import { SafeAreaView, StatusBar } from 'react-native'

import {
  NavigationContainer, DefaultTheme,
  DarkTheme,
} from '@react-navigation/native';
import MainNavigator from './MainNavigator';
import { useTheme } from '../theme/Hooks'
import { navigationRef } from './utils'
import { useDispatch } from 'react-redux'
import { changeTheme } from '../redux/reducer/slice.reducer';

export default function Navigation() {
  const dispatch = useDispatch()
  const {NavigationTheme,darkMode } = useTheme();
  const {colors}=NavigationTheme;
  const onChangeTheme = ({ theme, darkMode }) => {
    dispatch(changeTheme({ theme, darkMode }))
  }
  useEffect(() => {
    onChangeTheme({ darkMode: true })
  }, []);
  return (
    <NavigationContainer theme={NavigationTheme} ref={navigationRef} >
      <StatusBar barStyle={darkMode ? 'light-content' : 'dark-content'} />
      <MainNavigator />
    </NavigationContainer>
  );
}
