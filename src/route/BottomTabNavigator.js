/* eslint-disable react-native/no-inline-styles */
/* eslint-disable sort-keys */
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import React from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { Colors } from "../components/Common/Style";
import { useTheme } from '../theme/Hooks'
import { navigationRef } from './utils'

import FeedScreen from '../screens/Feed/stack';
import MarketScreen from '../screens/Market/stack';
import HomeScreen from '../screens/home/stack';
import CommunityScreen from '../screens/Community/stack';
import ProfileScreen from '../screens/profile/stack';
const BottomTabNavigator = ({ navigation, route }) => {
  const Tab = createBottomTabNavigator();
  const { darkMode, NavigationTheme } = useTheme();
  const { colors } = NavigationTheme
  return (
    <Tab.Navigator
      initialRouteName="HomeScreen"
      tabBarOptions={{
        inactiveTintColor: '#DADADA',
        showLabel: true,
        keyboardHidesTabBar: Platform.OS !== 'ios',
        style: {
          backgroundColor: '#fff',
          // paddingBottom:20,
          minHeight: 80,
        },
      }}
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          switch (route.name) {
            case 'Home':

              return (
                <MaterialIcons
                  style={{ alignSelf: 'center' }}
                  name="home"
                  size={30}
                  color={focused ? "#32BEA6" : colors.text}

                />
              )
            case 'Market':
              return (
                <FontAwesome5
                  style={{ alignSelf: 'center' }}
                  name="store"
                  size={22}
                  color={focused ? "#32BEA6" : colors.text}

                />
              )
            case 'Feed':
              return (
                <Entypo
                  style={{ alignSelf: 'center' }}
                  name="image-inverted"
                  size={26}
                  color={focused ? "#32BEA6" : colors.text}

                />)
            case 'Community':
              return (
                <FontAwesome5
                  style={{ alignSelf: 'center' }}
                  name="users"
                  size={24}
                  color={focused ? "#32BEA6" : colors.text}

                />)
            case 'Profile':
              return (
                <FontAwesome5
                  style={{ alignSelf: 'center' }}
                  name="user-alt"
                  size={24}
                  color={focused ? "#32BEA6" : colors.text}

                />)
            default:
              return (
                <FontAwesome5
                  style={{ alignSelf: 'center' }}
                  name="users"
                  size={26}
                  color={focused ? "#32BEA6" : colors.text}

                />)


          }
        },
        tabBarLabel: ({ focused, color }) => {
          return <Text style={{ color: focused ? "#32BEA6" : colors.text, fontSize: 8, fontWeight: 'bold' }}>{((route.name)).toUpperCase()}</Text>
        },
      })}
    >
      <Tab.Screen name="Home" component={HomeScreen} options={{
        headerShown: false
      }} />
      <Tab.Screen name="Market" component={MarketScreen} options={{
        headerShown: false
      }} />
      <Tab.Screen name="Feed" component={FeedScreen} options={{
        headerShown: false
      }} />
      <Tab.Screen name="Community" component={CommunityScreen}
        options={{
          headerShown: false
        }}
      />
      <Tab.Screen name="Profile" component={ProfileScreen} options={{
        headerShown: false
      }} />

    </Tab.Navigator>
  );
};
export default BottomTabNavigator;
const styles = StyleSheet.create({});
