/* eslint-disable no-sparse-arrays */
import { Dimensions } from 'react-native';
export const DeviceHeight = Dimensions.get('window').height;
export const DeviceWidth = Dimensions.get('window').width;
export const imageslist = [
  { image: require('../assets/modal/1.png') },
  { image: require('../assets/modal/2.png') },
  { image: require('../assets/modal/3.png') },
  { image: require('../assets/modal/4.png') },
  { image: require('../assets/modal/5.png') },
  { image: require('../assets/modal/6.png') },
  { image: require('../assets/modal/7.png') },
  { image: require('../assets/modal/8.png') },
  { image: require('../assets/modal/9.png') },
  { image: require('../assets/modal/10.png') },
  { image: require('../assets/modal/11.png') },
  { image: require('../assets/modal/12.png') },
  { image: require('../assets/modal/13.png') },
];
export const ModalList = [
  { image: require('../assets/modal/1.png') },
  { image: require('../assets/modal/2.png') },
  { image: require('../assets/modal/3.png') },
  { image: require('../assets/modal/4.png') },
  { image: require('../assets/modal/5.png') },
  { image: require('../assets/modal/6.png') },
  { image: require('../assets/modal/7.png') },
  { image: require('../assets/modal/8.png') },
  { image: require('../assets/modal/9.png') },
  { image: require('../assets/modal/10.png') },
  { image: require('../assets/modal/11.png') },
  { image: require('../assets/modal/12.png') },
  { image: require('../assets/modal/13.png') },

];
export const GallaryList = [
  { image: require('../assets/Img/1.png') },
  { image: require('../assets/Img/2.png') },
  { image: require('../assets/Img/3.png') },
  { image: require('../assets/Img/4.png') },
  { image: require('../assets/Img/5.png') },
  { image: require('../assets/Img/6.png') },
  { image: require('../assets/Img/7.png') },
  { image: require('../assets/Img/8.png') },
  { image: require('../assets/Img/9.png') }, 
   { image: require('../assets/Img/10.png') },
   { image: require('../assets/Img/11.png') },
   { image: require('../assets/Img/12.png') },





];
export const localImage = {
  art: { image: require('../assets/Img/art.png') },
  digital: { image: require('../assets/Img/digital.png') },
  loading:{ image: require('../assets/loading.gif') },
  backgroundImage:require('../assets/Img/backgraound.png'),
  music:require('../assets/Img/music.png'),
  Frame:require('../assets/Img/Frame.png'),
  tournment:require('../assets/Img/tournment.png'),
  server:require('../assets/Img/server.png'),
};
export const userInfo = {
  name: 'Xyz ',
  email: 'abc@gmail.com',
  comment: 'Why Netflix & Chill when you can NFT & Chill? 😎',
};
export const userNftList = [
  { name: 'User Created', id: 1 },
  { name: 'User Purchase', id: 2 },
  { name: 'User Unlisted', id: 3 },
  { name: 'My Collection', id: 4 },
  { name: 'Favarotes', id: 5 },
];
export const Colors = {
  red00: 'red ',
  blue: 'blue',
  black: 'black',
};
