/* eslint-disable react-native/no-color-literals */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable sort-keys */
import React, { Component } from 'react';
import {
  BackHandler,
  Dimensions,
  Image,
  Platform,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import { Colors, DeviceHeight, DeviceWidth } from '../Common/Style';
import { moderateScale, scale, verticalScale } from '../Scale';
import { useTheme } from '../../theme/Hooks'

export const Header = (props) => {
  const {  NavigationTheme } = useTheme();
  const {colors}=NavigationTheme;
  return (
    <View>

      <View style={{ padding: 10, borderBottomWidth:0.5,borderColor:'#363636' }}>
        <View style={{ marginTop: Platform.OS === 'ios' ? 30 : 0, flexDirection: 'row',alignItems:'center' }}>
          {/* <View style={{ flex: 0.1 }}>

            <TouchableOpacity
              onPress={() => props.navigation.toggleDrawer()}
              style={{
                right: 10,
              }}>
              <Icon1 name="menu" size={35} color={colors.text} />
            </TouchableOpacity>
          </View> */}
          <View style={{ flex: 0.8 }}>
          <Image
            style={{ height: 60,width:DeviceWidth/2}}
            resizeMode="stretch"
            source={require('../../assets/Img/logo.png')}
          />
            </View>
            <View style={{ flex: 0.1 }}>
            <TouchableOpacity
              onPress={() =>props.navigation.navigate('Notification')}
            >
              <FontAwesome name="bell" size={25} color={colors.text} />
            </TouchableOpacity>
            </View>
            <View style={{ flex: 0.1 }}>
            <TouchableOpacity
              onPress={() =>props.navigation.navigate('Wallet')}
            >
              <FontAwesome5 name="wallet" size={25} color={colors.text} />
            </TouchableOpacity>
            </View>
        </View>
      </View>
    </View>

  );
}

export const HeaderWithBack = (props) => {
  const {  NavigationTheme } = useTheme();
  const {colors}=NavigationTheme;
  return (
    <View>

      <View style={{ backgroundColor: colors.card,padding: 5 }}>
        <View style={{ marginTop: Platform.OS === 'ios' ? 30 : 0, flexDirection: 'row', }}>
          <View style={{ flex: 0.1, alignSelf: 'center' }}>

            <TouchableOpacity
              onPress={() => props.navigation.goBack()}
              style={{ width: 100 }}>
              <FontAwesome name="arrow-left" size={25} color={colors.text} />
            </TouchableOpacity>
          </View>
          <View style={{ flex: 0.8, justifyContent: 'center' }}>
            <Text style={{
              color: colors.text,
              fontSize: 20,
              fontWeight: 'bold',
              marginTop: -5,
            }}>{props.name} </Text>


          </View>
        </View>
      </View>

    </View >
  );
}



