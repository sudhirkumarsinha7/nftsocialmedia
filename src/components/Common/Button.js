import React, { useEffect, useState, useRef } from 'react';
import { Button, FlatList, Image, SafeAreaView, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View, Platform, } from 'react-native';

import { Colors, DeviceWidth, CommonStyle } from '../Common/Style';
import { moderateScale, scale, verticalScale } from "../Scale";
import LinearGradient from 'react-native-linear-gradient';
import { join } from 'path';


export const CustomLinearBotton =(props)=>{
    return  <LinearGradient
    start={{ x: 0, y: 0 }}
    end={{ x: 1, y: 0 }}
    colors={['#0038F5', '#9F03FF']}

    style={{
      borderRadius: scale(10),
      padding: scale(13),
      margin:scale(10)

    }}>
        <TouchableOpacity  onPress={props.onNavigate}>
        <Text style={{color:Colors.whiteFF,fontSize:scale(18),textAlign:'center',fontWeight:'bold'}}>{props.title}</Text>
    </TouchableOpacity>
    </LinearGradient>
}
export const CustomLinearBottonCircle =(props)=>{
  return  <LinearGradient
  start={{ x: 0, y: 0 }}
  end={{ x: 1, y: 0 }}
  colors={['#0038F5', '#9F03FF']}

  style={{
    borderRadius: scale(20),
    height:scale(40),
    width:scale(40),
    alignItems:'center',
    justifyContent:'center'
    
    
  }}>
      <TouchableOpacity  onPress={props.onNavigate} disabled={true}>
      <Text style={{color:Colors.whiteFF,fontSize:scale(18),textAlign:'center',fontWeight:'bold'}}>{props.title}</Text>
  </TouchableOpacity>
  </LinearGradient>
}
export const CustomLinearBotton1 =(props)=>{
    return  <LinearGradient
    start={{ x: 0.0, y: 1.0 }} 
    end={{ x: 1.0, y: 1.0 }}
    colors={['#0038F5', '#9F03FF']}

    style={{
      borderRadius: scale(10),
      padding: scale(2),
      margin:scale(10)

    }}>
        <TouchableOpacity  onPress={props.onNavigate} style={{
        backgroundColor: Colors.black0,
        borderRadius: scale(10),
        padding:scale(10)
        }}>
        <Text style={{color:Colors.whiteFF,fontSize:scale(18),textAlign:'center',fontWeight:'bold'}}>{props.title}</Text>
    </TouchableOpacity>
    </LinearGradient>
}

export const CustomLinearBotton2 =(props)=>{
  return  <LinearGradient
  start={{ x: 0, y: 0 }}
  end={{ x: 1, y: 0 }}
  colors={props.isColorful?['#0038F5', '#9F03FF']:['gray', 'gray']}

  style={{
    borderRadius: scale(20),
   padding:5,
    margin:scale(10)

  }}>
      <TouchableOpacity  onPress={props.onNavigate}>
      <Text style={{color:Colors.whiteFF,fontSize:scale(14),textAlign:'center',fontWeight:'bold', paddingLeft: scale(10),paddingRight: scale(10)}}>{props.title}</Text>
  </TouchableOpacity>
  </LinearGradient>
}