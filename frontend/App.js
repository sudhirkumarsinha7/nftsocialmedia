/* eslint-disable react-native/no-inline-styles */

import 'react-native-gesture-handler';
import * as React from 'react';
import { StatusBar, Text, View, Platform } from 'react-native';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/lib/integration/react'

// import Store from '../src/redux/Store';
import Root from '../src/route/Navigation';
import { Store, persistor } from '../src/redux/reducer'

import { WebView } from 'react-native-webview';


function App() {
  return (
    <Provider store={Store}>
     <PersistGate loading={null} persistor={persistor}>
          <Root />
    </PersistGate>
    </Provider>
  );
}
// function App() {

//   return <WebView source={{ uri: 'https://test.tollytokens.com/' }}
//     style={{ marginTop: Platform.OS === 'ios' ? 40 : 0 }} />

// }

export default App;
